<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Representative;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function index(){
        $categories = Category::latest()->get();
        return view('category.index',compact('categories'));
    }

    /*
     * Return representative data
     *
     * */


    /*public function get_category_data(Request $request){
        //dump('ok');
        if ($request->ajax()) {
            //dump('ok');
            //$data = DB::table('category')->select(DB::raw('@rownum  := @rownum  + 1 AS rownum'),'id','name');
            DB::statement(DB::raw('set @rownum=0'));
            $data = DB::table('category')->select(DB::raw('@rownum  := @rownum  + 1 AS rownum'),'id','name');
            //return $data;
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<div class="btn-group" role="group" aria-label="Basic example">
                                            <a class="btn bg-warning btn-sm" href="javascript:void(0);" onclick="editRepresentative('.$row->id.');">
                                                                 Edit</a>
                                            <a class="btn bg-danger btn-sm" href="javascript:void(0);" onclick="deleteRepresentative('.$row->id.');">
                                                                Delete</a>
                                           </div>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }*/



    /*
     * representative view
     *
     * */


    public function create_category(){
        return view('category.create');
    }


    /*
     * Store representative data
     *
     * */

    public function store_category(Request $request){
        //dd($request->all());

        $request->validate([
            'name'=>'required|unique:category',
        ]);

        //$input = $request->all();
        //Category::create($input);
        $category = new Category();
        $category->name = $request->name;
        $category->save();
        return back()->with('success','Category create successfully');
    }


    /*
     * Update representative by id
     *
     * */

    public function get_category_by_id($id){
        $category = Category::find($id);
        return response()->json($category);
    }

    public function update_category(Request $request){
        $request->validate([
            'name'=>'required',
        ]);

        $representative = Category::find($request->id);
        $representative->name = $request->name;
        $representative->save();
        $message = "Data update successfully";
        return response()->json(['message' => $message], 200);
    }


    /*
     * Delete representative data
     * */

    public function delete_category($id){

        $category = Category::find($id);
        $category->delete();
        return response()->json(['message'=>'Data delete successfully']);
    }
}
