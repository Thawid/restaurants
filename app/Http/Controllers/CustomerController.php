<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use DataTables;

class CustomerController extends Controller
{
    public function __construct(){

        $this->middleware('auth');
    }


    /*
     * Customer list view
     * */

    public function index(){
        return view('customer.index');
    }

    /*
     * Ger all customer data
     * */
    public function getAllCustomer(Request $request){
        //dd('hello');
        if ($request->ajax()) {
            $data = Customer::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $actionBtn = '<div class="btn-group" role="group" aria-label="Basic example">
                                            <a class="btn bg-warning btn-sm" href="javascript:void(0);" onclick="editCustomer('.$row->id.');">
                                                                 Edit</a>
                                            <a class="btn bg-danger btn-sm" href="javascript:void(0);" onclick="deleteCustomer('.$row->id.');">
                                                                Delete</a>
                                           </div>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /*
    * Add new customer view
    *
    * */
    public function create_customer(){
        return view('customer.create');
    }
    /*
     * Store new customer data
     *
     * */

    public function store(Request $request){
        //dd($request->all());

        $request->validate([
            'customer_name'=>'required|unique:customers',
            'phone'=>'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:11|max:14|unique:customers',
            'email'=>'nullable|unique:customers',
        ]);

        $input = $request->all();
        $customer = Customer::create($input);
        return back()->with('success','Customer create successfully');
    }

    /*
     * add customer form pos
     * */

    public function add_customer(Request $request){

        $request->validate([
            'customer_name'=>'required|unique:customers',
            'phone'=>'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:11|max:14|unique:customers'
        ]);

        $customer = new Customer();
        $customer->customer_name = $request->customer_name;
        $customer->phone = $request->phone;
        $customer->email = $request->email;
        $customer->address = $request->address;
        $customer->save();

        $message = "Customer added successfully";

        return response()->json([
            'message' => $message,
            'customer_id'=>$customer->id
        ], 200);
    }

    /*
     * Find customer by id
     *
     * */
    public function get_customer_by_id($id){

        $customer = Customer::find($id);
        return response()->json($customer);
    }

    /*
     * Update customer information
     *
     * */
    public function update_customer(Request $request){

        $request->validate([
            'customer_name'=>'required',
            'phone'=>'nullable|regex:/^([0-9\s\-\+\(\)]*)$/|min:11|max:14',
            'email'=>'nullable'
        ]);

        $customer = Customer::find($request->id);

        $customer->customer_name = $request->customer_name;
        $customer->phone = $request->phone;
        $customer->email = $request->email;
        $customer->address = $request->address;
        $customer->save();

        $message = "Customer data update successfully";
        return response()->json(['message' => $message], 200);
    }

    /*
     * Delete customer method
     * */
    public function delete_customer($id){

        $data = Customer::find($id);
        $data->delete();
        return response()->json(['message'=>'Customer delete successfully']);
    }
}
