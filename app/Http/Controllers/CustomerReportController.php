<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\PosSetting;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Mpdf\Tag\Input;


class CustomerReportController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
     * Customer report view
     *
     * */
    public function index()
    {
        $customers = DB::table('customers')->select('id', 'customer_name')->get();
        return view('report.customer', compact('customers'));
    }


    /*
     * Get data using ajax request
     *
     * */

    public function get_data($id, $start_date, $end_date)
    {

        if ($id == 0 || $start_date == '' || $end_date == '') {
            return response()->json(['errors' => 'All Field Should be required'], 400);
        } else {
            $arr['data'] = DB::table('invoices')
                ->join('medicines', 'medicines.id', 'invoices.product_id')
                ->select(DB::raw('medicines.name,medicines.packing,SUM(invoices.product_qty) as qty, SUM(invoices.product_bonus) as bonus, medicines.price'))
                ->whereBetween('invoices.created_at', [$start_date, $end_date])
                ->where('invoices.customer_id', $id)
                ->groupBy('invoices.product_id')
                ->get();
        }
        //dd($arr);
        echo json_encode($arr);
        exit;
    }


    public function customer_report()
    {
        $customers = DB::table('customers')->select('id', 'customer_name')->get();
        return view('report.customer-report', compact('customers'));
    }


    public function get_customer_report(Request $request)
    {
        //dd($request->data['person_id']);
        $id = $request->data['person_id'];
        $start_date = $request->data['start_date'];
        $start_date = $start_date. ' 00:00:00';
        $end_date = $request->data['end_date'];
        if(empty($end_date)){
            $end_date = $request->data['start_date'];
        }
        $end_date = $end_date. ' 11:59:59';

        $customer = Customer::where('id','=',$id)->first();
        $company_info = PosSetting::select('company_name','phone','email','web_site','address')->first();
        $customer_report = DB::table('invoices')
            ->join('medicines', 'medicines.id', '=', 'invoices.product_id')
            ->select(DB::raw('medicines.name,medicines.packing,SUM(invoices.product_qty) as qty, SUM(invoices.product_bonus) as bonus, medicines.price'))
            ->whereBetween('invoices.created_at', [$start_date, $end_date])
            ->where('invoices.customer_id', $id)
            ->groupBy('invoices.product_id')
            ->get();

        return view('report.customer-report-view', compact('customer_report','customer','company_info'));


    }

}
