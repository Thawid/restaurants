<?php

namespace App\Http\Controllers;

use App\Models\Loginlog;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $total_customer = $this->total_customer();
        $new_customer = $this->new_customer();
        $total_medicine = $this->total_medicine();
        $today_bill = $this->today_bill();
        $login_info = $this->login_information();
        return view('home',compact('total_customer','new_customer','total_medicine','today_bill','login_info'));
    }

    /*
     * Return number of total customer
     *
     * */
    private function total_customer(){
        $total_customer = DB::table('customers')->select(DB::raw('COUNT(id) as total_customer'))->first();
        return $total_customer;
    }

    /*
     * Return total number of new customer
     *
     * */

    private function new_customer(){

        $new_customer = DB::table('customers')->select(DB::raw('COUNT(id) as new_customer'))
            ->whereDate('created_at', Carbon::today())
            ->first();

        return $new_customer;
    }

    /*
     * Return total number of medicine
     *
     * */

    private function total_medicine(){
        $total_medicine = DB::table('medicines')->select(DB::raw('COUNT(id) as total_medicine'))->first();
        return $total_medicine;
    }

    /*
     * Today total bill
     *
     * */

    private function today_bill(){
        $today_bill = DB::table('sells')
            ->select(DB::raw('SUM(grand_total) as today_bill'))
            ->whereDate('created_at',Carbon::today())
            ->first();

        return $today_bill;
    }


    /*
     * Return last five login information
     *
     * */


    private function login_information(){
        $login_info = Loginlog::orderBy('id','DESC')->take(5)->get();
        return $login_info;
    }
}
