<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use App\Models\PosSetting;
use App\Models\Product;
use App\Models\Representative;
use App\Models\Sell;
use App\Models\VatSetting;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Medicine;
use App\Models\Customer;
use DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rules\In;
use PDF;

class InvoiceController extends Controller
{


    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
     * Make Invoice Page
     *
     * */

    public function index()
    {
        $customer_list = $this->customer_list();
        $representative_list = $this->representative_list();
        $vat = VatSetting::latest()->value('vat');
        return view('invoice.index')->with(compact('customer_list','representative_list','vat'));
    }



    /*
     * Invoice list table
     *
     * */

    public function invoice_list()
    {
        return view('invoice.invoice-list');
    }

    /*
     * Get invoice list data
     *
     * */

    public function get_invoice_list(Request $request)
    {
        if ($request->ajax()) {
            DB::statement(DB::raw('set @rownum=0'));
            $data = DB::table('customers')
                ->join('sells', 'sells.customer_id', '=', 'customers.id')
                ->select(DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'customers.id', 'customers.customer_name', 'sells.id as sell_id', 'sells.invoice_no', 'sells.total_qty', 'sells.vat', 'sells.total_product_price','sells.grand_total','sells.discount_amount','sells.receiveable', 'sells.paid as payment_status','sells.created_at');
            //dd($data);
            return DataTables::of($data)
                ->addColumn('delete', function ($row) {
                    $checkBox = '<input type="checkbox" name="ids" class="checkBoxClass" value="'.$row->sell_id.'">';
                    return $checkBox;
                })
                ->addColumn('total_product_price', function ($row) {
                    return number_format($row->total_product_price,2);
                })
                ->addColumn('vat', function ($row) {
                    return number_format($row->vat,2);
                })
                ->addColumn('grand_total', function ($row) {
                    return number_format($row->grand_total,2);
                })
                ->addColumn('discount_amount', function ($row) {
                    return number_format($row->discount_amount,2);
                })
                ->addColumn('receiveable', function ($row) {
                    return number_format($row->receiveable,2);
                })
                ->addColumn('payment_status', function ($row) {
                    return ($row->payment_status > 0) ? 'Paid' : 'Due';
                })
                ->addColumn('action', function ($row) {
                    $actionBtn = '<div class="btn-group" role="group" aria-label="Basic example">
                                            <a class="btn bg-success btn-sm" href="javascript:void(0);" onclick="getPayment('.$row->sell_id.');">
                                                Pay</a>
                                                <a href="' . action('App\Http\Controllers\InvoiceController@invoice_update_view', ['id' => $row->sell_id]) . '" type="button" class="btn bg-warning btn-sm">Edit</a>
                                            <a href="' . action('App\Http\Controllers\InvoiceController@print', ['id' => $row->sell_id]) . '" target="_blank" class="btn btn-primary btn-sm">Print</a>

                                        </div>';
                    return $actionBtn;
                })
                ->setRowClass(function ($row) {
                    return 'sell_' . $row->sell_id;
                })
                ->setRowId(function ($row) {
                    return 'sid' .$row->sell_id;
                })
                ->rawColumns(['action','delete'])
                ->make(true);
        }
    }


    /*
     * Medicine list for add to invoice
     *
     * */
    public function medicine_list(Request $request)
    {

        if ($request->ajax()) {
            //$data = Medicine::latest()->get();
            $data = DB::table('medicines')->select(['id', 'name', 'packing', 'price']);
            //dd($data);
            return DataTables::of($data)
                ->addIndexColumn()

                ->addColumn('mergeColumn', function ($row) {
                    return $row->packing;
                })
                ->addColumn('action', function ($row) {
                    $actionBtn = '<a href="javascript:void(0);" onclick="add_product_to_chart(' . $row->id . ',this,' . $row->id . ')"><i class="fa fa-cart-plus" aria-hidden="true"></i></a>';
                    return $actionBtn;
                })
                ->setRowClass(function ($row) {
                    return "product_" . $row->id;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /*
     * Customer list for invoice
     *
     * */
    private function customer_list()
    {
        $customer = Customer::select('id', 'customer_name')->get();
        return $customer;
    }

    /*
     * Return representative list
     *
     * */

    private function representative_list(){

        $representative = Representative::select('id','name')->get();
        return $representative;
    }

    /*
     * Save all invoice data
     *
     * */


    public function save_invoice_data(Request $request)
    {
        //dd($request->all());
        $has_error = $this->validate_invoice_data($request->all());
        if ($has_error) {
            return redirect()->back()->withErrors($has_error);
        } else {
            $customer_id = $request->input('customer_id');
            $table_id = $request->input('table_id');
            $product_list = $request->input('product_id');
            $product_qty = $request->input('product_qty');
            $product_price = $request->input('product_price');
            $total_qty = $request->input('total_qty');
            $total_product_price = $request->input('total_product_price');
            $vat = $request->input('vat');
            $grand_total = $request->input('grand_total');
            $paid = $request->input('paid');
            $payable = $request->input('payable');
            $receiveable = $request->input('receiveable');
            $discount = $request->input('discount');
            $discount_amount = $request->input('discount_amount');
            $counter = 0;
            $total = 0;
            $invoice_no = date("his");
            //dd($paid);
            foreach ($product_list as $product_id) {
                $price = $this->get_product_price($product_id);
                $qty = $product_qty[$counter];
                $total = $total + ($qty * $product_price[$counter]);
                $counter++;
            }
            if ($total != $total_product_price) {
                return redirect()->back()->withErrors("Input total price dosen't match");
            } else {
                DB::beginTransaction();
                try {
                    $sell = Sell::create([
                        'customer_id' => $customer_id,
                        'table_id' => $table_id,
                        'invoice_no' => $invoice_no,
                        'total_qty' => $total_qty,
                        'total_product_price' => $total_product_price,
                        'vat' =>$vat,
                        'grand_total'=>$grand_total,
                        'paid' =>$paid,
                        'discount' =>$discount,
                        'discount_amount' =>$discount_amount,
                        'payable'=>$payable,
                        'receiveable'=>$receiveable
                    ]);
                    $sell_id = $sell->id;
                    $counter = 0;
                    $insert_row = array();

                    try {

                        foreach ($product_list as $product_id) {
                            $insert_row[] = [
                                'product_id' => $product_id,
                                'product_qty' => $product_qty[$counter],
                                'product_price' => $product_price[$counter],
                                'customer_id' => $customer_id,
                                'table_id' => $table_id,
                                'sell_id' => $sell_id,
                                'status' => 1,
                                'created_at' => Carbon::now(),
                                'updated_at' => Carbon::now(),
                            ];
                            $counter++;
                        }

                        try {
                            Invoice::insert($insert_row);
                            DB::commit();
                            return redirect()->route('invoice.pos.details', $sell_id);
                            //return redirect()->back()->with('message', 'Invoice created successfully');
                        } catch (\Exception $e) {
                            DB::rollBack();
                            return redirect()->back()->withErrors("Invoice dosen't make complete" . $e->getMessage());
                        }

                    } catch (\Exception $e) {
                        DB::rollBack();
                        return redirect()->back()->withErrors("Invoice dosen't make complete" . $e->getMessage());
                    }

                } catch (\Exception $e) {
                    DB::rollBack();
                    return redirect()->back()->withErrors("Invoice dosen't make complete" . $e->getMessage());
                }
            }
        }
    }


    /*
     * Validation new invoice data
     *
     * */

    private function validate_invoice_data($request)
    {

        $validator = Validator::make($request, [
            'customer_id' => 'required|exists:customers,id',
            'product_id' => 'required|array',
            'product_qty' => 'required|array',
            'product_qty.*' => 'required|numeric',
            'product_price' => 'required|array',
            'total_qty' => 'required|numeric',
            'total_product_price' => 'required|numeric',
            'product_id.*' => 'required|exists:medicines,id'
        ], [
            'customer_id.exists' => 'Customer not found',
            'product_id.array' => 'Invalid Medicine list',
            'product_qty.array' => 'Invalid Medicine quantity',
            'product_id.*.exists' => 'Medicine not found',
            'product_qty.*.numeric' => 'Invalid Medicine qty data',
            'total_product_price.required' => 'Total price is required',
            'total_product_price.numeric' => 'Invalid total price',
        ]);

        if ($validator->fails()) {
            return $validator->errors()->all();
        } else {
            return false;
        }
    }

    /*
     * Get product price for match invoice product price
     *
     * */


    private function get_product_price($product_id)
    {

        return Medicine::where(['id' => $product_id])->select('id', 'price')->first();
    }


    /*
     * Invoice view
     *
     * */


    public function invoice_view($sell_id)
    {
        $sells_item = Sell::with(['customer', 'invoice'])->where('id', $sell_id)->first();
        //dd($sells_item);
        if ($sells_item) {
            return view('invoice.invoice-details', ['sells_item' => $sells_item]);
        } else {
            return redirect()->back()->withErrors('Sorry, Invoice not found');
        }
    }


    /*
     * Invoice POS View
     * */

    public function invoice_pos_view($sell_id)
    {
        $store_info = PosSetting::select('company_name','address','phone','email','web_site','footer_text','vat_reg')->first();
        $sells_item = Sell::with(['customer', 'invoice','representative'])->where('id', $sell_id)->first();
        $vat = VatSetting::latest()->value('vat');
        //dd($sells_item);
        if ($sells_item) {
            return view('invoice.pos-view', ['sells_item' => $sells_item, 'store_info'=>$store_info,'vat'=>$vat]);
        } else {
            return redirect()->back()->withErrors('Sorry, Invoice not found');
        }
    }

    /*
     * Print POS Invoice
     * */

    public function print($id){
        $vat = VatSetting::latest()->value('vat');
        $store_info = PosSetting::select('company_name','address','phone','email','web_site','footer_text','vat_reg')->first();
        $sells_item = Sell::with(['customer', 'invoice'])->where('id', $id)->first();
        //dd($sells_item);
        if ($sells_item) {
            return view('invoice.pos-print', ['sells_item' => $sells_item, 'store_info'=>$store_info,'vat'=>$vat]);
        } else {
            return redirect()->back()->withErrors('Sorry, Invoice not found');
        }
    }

    /*
     * Invoice PDF view
     *
     * */
    public function invoice_pdf_view($invoice_id)
    {

        $sells_item = Sell::with(['customer', 'invoice','representative'])->where('id', $invoice_id)->first();
        if ($sells_item) {

            $pdf = PDF::loadView('invoice.invoice-pdf', ['sells_item' => $sells_item]);
            $pdf->SetProtection(['copy', 'print'], '', 'pass');
            return $pdf->stream($sells_item->customer->customer_name . '.pdf');
        } else {
            return redirect()->back()->withErrors('Sorry, Invoice not found');
        }

    }


    /*
     * Print Invoice
     *
     * */

    public function get_invoice_print($invoice_id)
    {
        $sells_item = Sell::with(['customer', 'invoice','representative'])->where('id', $invoice_id)->first();
        if ($sells_item) {
            return view('invoice.print-invoice', ['sells_item' => $sells_item]);
            //return view('print.pages.invoice',['quotation'=>$sells_item]);
        } else {
            return redirect()->back()->withErrors('Sorry, Invoice not found');
        }
    }

    /*
     * View update information
     *
     * */
    public function invoice_update_view($sell_id)
    {
        $customers = $this->customer_list();
        $representative_list = $this->representative_list();
        $vat = VatSetting::latest()->value('vat');
        $sells_item = Sell::with(['customer', 'invoice','representative'])->where('id', $sell_id)->first();
        //dd($sells_item);
        if ($sells_item) {
            return view('invoice.update-invoice', ['sells_item' => $sells_item, 'customers' => $customers,'representative_list'=>$representative_list,'vat'=>$vat]);
        } else {
            return redirect()->back()->withErrors('Sorry, Invoice not found');
        }
    }


    /*
     * Save update information
     *
     * */

    public function save_update_data(Request $request, $id)
    {
        //dd($request->all());
        $customer_id = $request->input('customer_id');
        $table_id = $request->input('table_id');
        $product_list = $request->input('product_id');
        $product_qty = $request->input('product_qty');
        $product_bonus = $request->input('product_bonus');
        $product_price = $request->input('product_price');
        $total_qty = $request->input('total_qty');
        $total_product_price = $request->input('total_product_price');
        $grand_total = $request->input('grand_total');
        $paid = $request->input('paid');
        $payable = $request->input('payable');
        $receiveable = $request->input('receiveable');
        $discount = $request->input('discount');
        $discount_amount = $request->input('discount_amount');
        $counter = 0;
        $total = 0;

        foreach ($product_list as $product_id) {
            $price = $this->get_product_price($product_id);
            $qty = $product_qty[$counter];
            $total = $total + ($qty * $product_price[$counter]);
            $counter++;
        }
        if ($total != $total_product_price) {
            return redirect()->back()->withErrors("Input total price dosen't match");
        } else {
            DB::beginTransaction();
            try {

                $sell_data = Sell::find($id);
                $sell_id = $sell_data->id;
                $sell_data->customer_id = $customer_id;
                $sell_data->table_id = $table_id;
                $sell_data->total_qty = $total_qty;
                $sell_data->total_product_price = $total_product_price;
                $sell_data->grand_total = $grand_total;
                $sell_data->paid = $paid;
                $sell_data->payable = $payable;
                $sell_data->receiveable = $receiveable;
                $sell_data->discount = $discount;
                $sell_data->discount_amount = $discount_amount;
                $sell_data->save();

                $counter = 0;
                $insert_row = array();

                try {
                    DB::table('invoices')->where('sell_id', $sell_id)->delete();
                    foreach ($product_list as $product_id) {
                        $insert_row[] = [
                            'product_id' => $product_id,
                            'product_qty' => $product_qty[$counter],
                            'product_price' => $product_price[$counter],
                            'customer_id' => $customer_id,
                            'table_id' => $table_id,
                            'sell_id' => $sell_id,
                            'status' => 1,
                            'created_at' => Carbon::now(),
                            'updated_at' => Carbon::now(),
                        ];
                        $counter++;
                    }

                    try {
                        Invoice::insert($insert_row);
                        DB::commit();
                        return redirect()->back()->with('message', 'Invoice update successfully');
                    } catch (\Exception $e) {
                        DB::rollBack();
                        return redirect()->back()->withErrors("Invoice dosen't updated" . $e->getMessage());
                    }

                } catch (\Exception $e) {
                    DB::rollBack();
                    return redirect()->back()->withErrors("Invoice dosen't make updated invoice table process" . $e->getMessage());
                }

            } catch (\Exception $e) {
                DB::rollBack();
                return redirect()->back()->withErrors("Invoice dosen't make updated sell table error" . $e->getMessage());
            }
        }
    }

    /*
     * get payment by id
     * */

    public function get_payment_by_id($id){
        $payment = Sell::find($id);
        return response()->json($payment);
    }

    /*
     * Save payment
     * */

    public function update_payment(Request $request){
       //dd($request->receiveable,$request->paid);
        $request->validate([
            'paid'=>'required',
        ]);
        if ($request->receiveable === $request->paid) {

            $representative = Sell::find($request->id);
            $representative->paid = $request->paid;
            $representative->save();
            $message = "Payment update successfully";
            return response()->json(['message' => $message], 200);
        } else {
            $message = "Paid amount must be equal to receiveable amount";
            return response()->json(['message' => $message], 400);
        }
    }

    /*
     * Delete Multiple Row With Select CheckBox
     * */

    public function delete_multiple(Request $request){

        //dd($request->all());
        $ids = $request->ids;
        $deleteSell = Sell::whereIn('id',$ids)->delete();
        $deleteInvoice = Invoice::whereIn('sell_id',$ids)->delete();
        if($deleteSell && $deleteInvoice){
        return response()->json(['message'=>'Invoice has been delete successfully'],200);
        } else {
            $message = "Can not delete";
            return response()->json(['message' => $message], 400);
        }
    }

    /*
     * Make a random string
     * */
    public function randomString($length = 2) {
        $str = "";
        $characters = array_merge(range('A','Z'), range('a','z'));
        $max = count($characters) - 1;
        for ($i = 0; $i < $length; $i++) {
            $rand = mt_rand(0, $max);
            $str .= $characters[$rand];
        }
        return $str;
    }



    /*
    * POS
    * */

    public function pos(){
        $customer_list = $this->customer_list();
        $representative_list = $this->representative_list();
        $vat = VatSetting::latest()->value('vat');
        return view('invoice.pos')->with(compact('customer_list','representative_list','vat'));
    }

    /*
     * Get product
     * */

    public function getProduct(Request $request){

        $products = [];

        if($request->has('q')){
            $search = $request->q;
            $products =Medicine::select("id", "name")
                ->where('name', 'LIKE', "%$search%")
                ->orWhere('packing', 'LIKE', "%$search%")
                ->get();
        }
        return response()->json($products);

    }
    public function addProduct(Request $request){

        //dd($request->all());
        $products =Medicine::select("id", "name",'packing','price')
            ->where('id',$request->product_id)
            ->get();
        //dd($products);
        return response()->json($products);

    }

}
