<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\Medicine;
use DataTables;
use Illuminate\Support\Facades\DB;

class MedicineController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    /*
     * Medicine list v
     *
     * @return void
     * */

    public function index(){
        $categories = Category::latest()->get();
        return view('medicine.index',compact('categories'));
    }

    /*
     * Get medicine list from DB
     *
     *
     * */

    public function get_medicine_list(Request $request){
        if ($request->ajax()) {
            //$data = Medicine::latest()->get();
            DB::statement(DB::raw('set @rownum=0'));
            $data = DB::table('medicines')->leftJoin('category','category.id','=','medicines.category')->select(DB::raw('@rownum  := @rownum  + 1 AS rownum'),'medicines.id','medicines.name','medicines.packing','medicines.price','category.name as category_name');
            //dd($data);
            return DataTables::of($data)
                ->addColumn('packing',function ($row){
                    return $row->packing;
                })
                ->addColumn('product_price',function ($row){
                    return $row->price;
                })
                ->addColumn('action', function($row){
                    $actionBtn = '<div class="btn-group" role="group" aria-label="Basic example">
                                            <a class="btn bg-warning btn-sm" href="javascript:void(0);" onclick="editMedicine('.$row->id.');">
                                                                 Edit</a>
                                            <a class="btn bg-danger btn-sm" href="javascript:void(0);" onclick="deleteMedicine('.$row->id.');">
                                                                Delete</a>
                                           </div>';
                    return $actionBtn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }
    }

    /*
     * Create new medicine
     *
     * @return void
     * */
    public function new_medicine(){
        $categories = Category::latest()->get();
        return view('medicine.create',compact('categories'));
    }

    /*
     * Store medicine information
     *
     * @return void
     * */
    public function store(Request $request){
        //dd($request->all());
        $request->validate([
            'name'=>"required|unique:medicines",
            'packing'=>"required|unique:medicines",
            'price'=>"required|regex:/^\d+(\.\d{1,2})?$/",
            'category'=>"required"
        ]);

        $input = $request->all();
        $medicine = Medicine::create($input);
        return back()->with('success','New item add successfully');
    }

    /*
     * Get medicine by medicine ID
     *
     * */

    public function get_medicine_by_id($id){
        $data = Medicine::leftJoin('category','category.id','=','medicines.category')
            ->select('medicines.id','medicines.name','medicines.category','category.id as cat_id','category.name as category_name','medicines.packing','medicines.price')
            ->where('medicines.id',$id)
            ->get();
        //$data = Medicine::with('category')->get();
        return response()->json($data[0]);
    }

    /*
     * Update medicine information
     *
     * */

    public function update_medicine(Request $request){
        //dd($request->all());
        $request->validate([
            'name'=>"required",
            'packing'=>"required",
            'price'=>"required"
        ]);

        $data = Medicine::find($request->id);

        $data->name = $request->name;
        $data->packing = $request->packing;
        $data->price = $request->price;
        $data->category = $request->category_name;
        $data->save();
        $message = "Data update successfully";
        return response()->json(['message'=>$message],200);
    }

    /*
     * Delete Medicine
     *
     * */

    public function delete_medicine($id){
        $data = Medicine::find($id);
        $data->delete();
        return response()->json(['message'=>'Successfully delete'],200);
    }

}
