<?php

namespace App\Http\Controllers;

use App\Models\PosSetting;
use Illuminate\Http\Request;

class PosSettingController extends Controller
{
    public function __construct(){

        $this->middleware('auth');
    }

    /*
    * Add new customer view
    *
    * */
    public function create_pos_settings(){
        $data = PosSetting::select('id','company_name','email','phone','address','web_site','footer_text','vat_reg')->first();
        return view('pos-setting.create',compact('data'));
    }

    /*
     * Update pos setting information
     *
     * */
    public function update_pos_setting(Request $request){
        //dd($request->all());
        $request->validate([
            'company_name'=>'required',
            'phone'=>'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:11|max:14',
            'address'=>'required',
        ]);

        $settings = PosSetting::find($request->id);
        if ($settings) {
            $settings->company_name = $request->company_name;
            $settings->phone = $request->phone;
            $settings->email = $request->email;
            $settings->address = $request->address;
            $settings->web_site = $request->web_site;
            $settings->vat_reg = $request->vat_reg;
            $settings->footer_text = $request->footer_text;
            $settings->save();
        } else {
            $input = $request->all();
            PosSetting::create($input);
        }
        return back()->with('success','Setting create successfully');

    }


}
