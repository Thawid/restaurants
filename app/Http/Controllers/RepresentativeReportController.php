<?php

namespace App\Http\Controllers;

use App\Models\PosSetting;
use App\Models\Representative;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RepresentativeReportController extends Controller
{


    public function index(){
        $representative_data = Representative::select('id','name')->get();
        return view('report.representative-report',compact('representative_data'));
    }


    public function get_representative_report(Request $request){

        $id = $request->data['representative_id'];
        $start_date = $request->data['start_date'];
        $start_date = $start_date. ' 00:00:00';
        $end_date = $request->data['end_date'];
        if(empty($end_date)){
            $end_date = $request->data['start_date'];
        }
        $end_date = $end_date. ' 11:59:59';

        $table = Representative::where('id',$id)->first();
        $company_info = PosSetting::select('company_name','phone','email','web_site','address')->first();
        $report_data =  DB::table('invoices')
            ->join('medicines','medicines.id','invoices.product_id')
            ->select(DB::raw('medicines.name,medicines.packing,SUM(invoices.product_qty) as qty, SUM(invoices.product_bonus) as bonus, medicines.price'))
            ->whereBetween('invoices.created_at',[$start_date,$end_date])
            ->where('invoices.table_id', $id)
            ->groupBy('invoices.product_id')
            ->get();

        return view('report.representative-report-view',compact('table','report_data','company_info'));
    }
}
