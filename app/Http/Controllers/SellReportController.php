<?php

namespace App\Http\Controllers;

use App\Models\PosSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SellReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /*
     * sell report view
     *
     * */

    public function sell_report()
    {
        return view('report.sell-report');
    }


    public function get_sell_report(Request $request)
    {
        //dd($request->data['person_id']);
        $start_date = $request->data['start_date'];
        $start_date = $start_date. ' 00:00:00';
        $end_date = $request->data['end_date'];
        if(empty($end_date)){
            $end_date = $request->data['start_date'];
        }
        $end_date = $end_date. ' 11:59:59';

        $company_info = PosSetting::select('company_name','phone','email','web_site','address')->first();
        $sell_report = DB::table('sells')
            ->whereBetween('sells.created_at', [$start_date, $end_date])
            ->get();

        return view('report.sell-report-view', compact('sell_report','company_info'));


    }
}
