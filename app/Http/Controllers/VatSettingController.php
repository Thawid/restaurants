<?php

namespace App\Http\Controllers;

use App\Models\VatSetting;
use Illuminate\Http\Request;

class VatSettingController extends Controller
{
    public function __construct(){

        $this->middleware('auth');
    }

    /*
    * Add new customer view
    *
    * */
    public function create_vat_settings(){
        $data = VatSetting::select('id','vat')->first();
        return view('vat-setting.create',compact('data'));
    }

    /*
     * Update pos setting information
     *
     * */
    public function update_vat_setting(Request $request){
        //dd($request->all());
        $request->validate([
            'vat'=>'required',
        ]);

        $settings = VatSetting::find($request->id);
        if ($settings) {
            $settings->vat = $request->vat;
            $settings->save();
        } else {
            $input = $request->all();
            VatSetting::create($input);
        }
        return back()->with('success','Vat create successfully');

    }
}
