<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    protected $table = "invoices";

    protected $fillable = [
        'product_id',
        'product_qty',
        'product_bonus',
        'product_price',
        'customer_id',
        'table_id',
        'sell_id',
        'status',
    ];


    /*
     * Every invoice has one medicine
     * */

    public function medicine(){
        return $this->hasOne('App\Models\Medicine','id','product_id');
    }


    /*
     * Get Invoice Sell Data
     *
     * */

    public function sell(){
        return $this->hasOne('App\Models\Sell','id','sell_id');
    }

    /*
     * Get Invoice customer data
     * */

    public function customer(){
        return $this->hasOne('App\Models\Customer','id','customer_id');
    }

    /*
     * Get Invoice representative data
     * */

    public function representative(){
        return $this->hasOne('App\Models\Representative','id','representative_id');
    }
}
