<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PosSetting extends Model
{
    use HasFactory;

    protected $table = 'pos_settings';

    protected $fillable = [
        'company_name',
        'email',
        'phone',
        'address',
        'web_site',
        'footer_text'
    ];
}
