<?php

function bangla($str) {
    $search=array("0","1","2","3","4","5",'6',"7","8","9");
    $replace=array("০",'১','২','৩','৪','৫','৬','৭','৮','৯');
    return str_replace($search,$replace,$str);
}

function en2bnNumber($amount, $dotted = false, bool $round = false)
{
    setlocale(LC_MONETARY, 'en_IN');
    if ($round) {

        //$number = money_format('%!i', round($amount));
        $number = round($amount);
    } else {
        //$number = money_format('%!i', $amount);
        $number = round($amount);
    }

    if (!$dotted) {
        $number = explode(".", $number);
        $number = $number[0];
    }
    $replace_array = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০", 'মধ্যাহ্ন', 'অপরাহ্ন','-');
    $search_array = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0", 'AM', 'PM','-');
    return str_replace($search_array, $replace_array, $number);
}
