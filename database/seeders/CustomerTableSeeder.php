<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(0,100) as $index){
            DB::table('customers')->insert([
                'customer_name'=>$faker->name('male'),
                'phone'=>$faker->phoneNumber,
                'address'=>$faker->address,
            ]);
        }

    }
}
