@extends('layout.main')

@section('style')
@endsection


@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">New Category </h1>
                    <a href="{{url('category-list')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="bi bi-people-fill"></i> Category List </a>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success" id="alert-success">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> {{ $message }}
                    </div>
                @endif
                @if($errors->any())
                    <div class="alert alert-danger" id="alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>Warning!</strong> {{ $error }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif

            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <p>
                            <small><i>* Every information given by the star must be provided correctly </i></small>
                        </p>
                        <form  action="{{route('store.category')}}" method="post">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="name"> Name * </label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" />
                                </div>
                            </div>
                            <button type="submit" id="submit" class="btn btn-success btn-sm"> Submit </button>


                        </form>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->
        </div>
    </div>
@endsection

@section('script')

    <script>

        $("#alert-success").fadeTo(2000, 500).slideUp(500, function(){
            $("#alert-success").slideUp(500);
        });

        $("#alert-danger").fadeTo(2000, 500).slideUp(500, function(){
            $("#alert-danger").slideUp(500);
        });

    </script>
@endsection
