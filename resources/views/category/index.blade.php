@extends('layout.main')
@section('style')

@endsection
@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0"> Category List </h1>
                    <a href="{{url('create-category')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="bi bi-people-fill"></i> নতুন ক্যাটাগরি </a>
                </div>
            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered category-list" id="" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th> SL </th>
                                    <th> Category Name </th>
                                    <th> Action </th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($categories as $category)
                                    <tr>
                                        <td> {{ $loop->iteration }}</td>
                                        <td> {{ $category->name }}</td>
                                        <td>
                                            <a class="btn bg-warning btn-sm" href="javascript:void(0);" onclick="editRepresentative({{$category->id}});">
                                                Edit</a>
                                            <a class="btn bg-danger btn-sm" href="javascript:void(0);" onclick="deleteRepresentative({{$category->id}});">
                                                Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->

            <!--  customer Modal Edit -->
            <div class="modal fade" id="categoryModalEdit">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="categoryModalEdit"> Update Information</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="update_category">
                                @csrf
                                <input type="hidden" id="id" name="id">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="name"> Category Name * </label>
                                        <input type="text" class="form-control" id="name" name="name"  />
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn bg-danger" data-dismiss="modal"> Cancel </button>
                                    <button type="submit" class="btn bg-success"> Save </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End customer Modal Edit -->
            <!--  customer Modal Delete -->
            <div class="modal fade" id="customerModalDelete" tabindex="-1" role="dialog" aria-labelledby="customerModalDelete" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="customerModalDelete"> Delete Category</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                           Do you want to delete this
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bg-warning" data-dismiss="modal">No </button>
                            <button type="button" class="btn bg-info">Yes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>

    <script>
        $(function () {

            var table = $('#category-list').DataTable({
                processing: true,
                serverSide: true,
                /*ajax: "{{ url('get_data') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'name', name: 'name'},
                    {data: 'action',name: 'action',orderable: false,searchable: false},
                ]*/
            });

        });
    </script>
    <script>
        function editRepresentative(id) {
            //console.log(id);
            $.get('category/'+id,function(category) {
                $("#id").val(category.id);
                $("#name").val(category.name);
                $("#categoryModalEdit").modal('toggle');
            });
        }

        $("#update_category").submit(function(e){

            e.preventDefault();
            toastr.options = {
                "closeButton": true,
                "newestOnTop": true,
                "positionClass": "toast-top-right"
            };
            let id = $("#id").val();
            let name = $("#name").val();
            let _token = $("input[name= _token]").val();
            $.ajax({
                url :"{{route('update.category')}}",
                type:"PUT",
                data:{
                    id:id,
                    name:name,
                    _token:_token,
                },
                success:function (response) {
                    if(response){
                        toastr.success(response.message);
                        $("#categoryModalEdit").modal('toggle');
                        $("#update_category")[0].reset();
                        location.reload();
                        //$('.category-list').DataTable().ajax.reload();

                    }

                },
                error: function(err){
                    toastr.error("Something went wrong !");
                    $("#categoryModalEdit").modal('toggle');
                    $("#update_category")[0].reset();
                }
            });
        });

        function deleteRepresentative(id){
            if(confirm('Do you realy want to delete this record?')){
                $.ajax({
                    url:'delete-category/'+id,
                    type:'get',
                    data:{
                        _token:$("input[name=_token]").val()
                    },
                    success:function(response){
                        toastr.success(response.message);
                        location.reload();
                        //$('.representative-list').DataTable().ajax.reload();
                    },
                    error:function (err){
                        toastr.error("Data can't be deleted !");

                    }
                });
            }
        }
    </script>

@endsection
