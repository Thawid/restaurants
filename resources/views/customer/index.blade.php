@extends('layout.main')
@section('style')

@endsection
@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">Customer List</h1>
                    <a href="{{url('customer-create')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="bi bi-people-fill"></i> Add New </a>
                </div>
            </div>
            <!-- page header -->



            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered yajra-datatable" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th> Customer Name</th>
                                        <th>Mobile No</th>
                                        <th> E-Mail </th>
                                        <th>Address</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->

            <!--  customer Modal Edit -->
            <div class="modal fade" id="customerModalEdit">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="customerModalEdit"> Update Customer Information </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>
                                <small><i>* Every information given by the star must be provided correctly </i></small>
                            </p>
                            <form id="update_customer">
                                @csrf
                                <input type="hidden" id="id" name="id">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="customer_name"> Customer Name * </label>
                                        <input type="text" class="form-control" id="customer_name" name="customer_name" placeholder="মোহাম্মাদ মেহেদী ইসলাম" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="phone"> Mobile No  </label>
                                        <input type="text" class="form-control" id="phone" name="phone" placeholder="01676000000" />
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="customerMobile"> E-Mail </label>
                                        <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com" />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <div class="form-group">
                                            <label for="address"> Address </label>
                                            <textarea class="form-control" id="address" name="address" placeholder="Customer address" rows="1"
                                            ></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn bg-danger" data-dismiss="modal"> Cancel </button>
                                    <button type="submit" class="btn bg-success"> Submit </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End customer Modal Edit -->
            <!--  customer Modal Delete -->
            <div class="modal fade" id="customerModalDelete" tabindex="-1" role="dialog" aria-labelledby="customerModalDelete" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="customerModalDelete"> Delete Customer </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            আপনি কি এই তথ্যটি মুছতে চান?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bg-warning" data-dismiss="modal">না</button>
                            <button type="button" class="btn bg-info">হ্যাঁ</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>

    <script>
        $(function () {

            var table = $('.yajra-datatable').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('customer.list') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'customer_name', name: 'customer_name'},
                    {data: 'phone', name: 'phone'},
                    {data: 'email', name: 'email'},
                    {data: 'address', name: 'address'},
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

        });
    </script>
    <script>
        function editCustomer(id) {
            $.get('customer/'+id,function(customer) {
                $("#id").val(customer.id);
                $("#customer_name").val(customer.customer_name);
                $("#phone").val(customer.phone);
                $("#email").val(customer.email);
                $("#address").val(customer.address);
                $("#customerModalEdit").modal('toggle');
            });
        }

        $("#update_customer").submit(function(e){

            e.preventDefault();
            toastr.options = {
                "closeButton": true,
                "newestOnTop": true,
                "positionClass": "toast-top-right"
            };
            let id = $("#id").val();
            let customer_name = $("#customer_name").val();
            let phone = $("#phone").val();
            let email = $("#email").val();
            let address = $("#address").val();
            let _token = $("input[name= _token]").val();
            $.ajax({
                url :"{{route('update.customer')}}",
                type:"PUT",
                data:{
                    id:id,
                    customer_name:customer_name,
                    phone:phone,
                    email:email,
                    address:address,
                    _token:_token,
                },
                success:function (response) {
                    if(response){
                        toastr.success(response.message);
                        $("#customerModalEdit").modal('toggle');
                        $("#update_customer")[0].reset();
                        $('.yajra-datatable').DataTable().ajax.reload();

                    }

                },
                error: function(err){
                    toastr.error("Something went wrong !");
                    $("#customerModalEdit").modal('toggle');
                    $("#update_customer")[0].reset();
                }
            });
        });

        function deleteCustomer(id){
            if(confirm('Do you realy want to delete this record?')){
                $.ajax({
                    url:'delete-customer/'+id,
                    type:'get',
                    data:{
                        _token:$("input[name=_token]").val()
                    },
                    success:function(response){
                        toastr.success(response.message);
                        $('.yajra-datatable').DataTable().ajax.reload();
                    },
                    error:function (err){
                        toastr.error("Data can't be deleted !");

                    }
                });
            }
        }
    </script>

@endsection
