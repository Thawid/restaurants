@extends('layout.main')

@section('body')
    <div class="container-fluid">
        <h1 class="mt-4">Dashboard</h1>

        <div class="card mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-warning text-white mb-4 text-center">
                            <div class="card-body">
                                <h5>New Customer</h5>
                                <h3> {{ $new_customer->new_customer ?? '0' }} </h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-primary text-white mb-4 text-center">
                            <div class="card-body">
                                <h5>Total products</h5>
                                <h3> {{ $total_medicine->total_medicine ?? '0' }}</h3>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-success text-white mb-4 text-center">
                            <div class="card-body">
                                <h5>Total Customer </h5>
                                <h3> {{ $total_customer->total_customer ?? '0' }}</h3>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-md-6">
                        <div class="card bg-info text-white mb-4 text-center">
                            <div class="card-body">
                                <h5>Today Sell</h5>
                                <h3>{{ number_format($today_bill->today_bill,2) ?? '0' }}  </h3>
                            </div>
                        </div>
                    </div>

{{--                    <div class="table-responsive">--}}
{{--                        <table class="table table-bordered due-report" id='medicineTable'>--}}
{{--                            <thead>--}}
{{--                            <tr>--}}
{{--                                <th> SL </th>--}}
{{--                                <th> Login Time </th>--}}
{{--                                <th> IP Address </th>--}}
{{--                            </tr>--}}
{{--                            </thead>--}}
{{--                            <tbody>--}}
{{--                            @php--}}
{{--                                $count = 1;--}}
{{--                            @endphp--}}
{{--                            @if(isset($login_info))--}}
{{--                                @foreach($login_info as $info)--}}
{{--                                    <tr>--}}
{{--                                        <td>{{$count++}}</td>--}}
{{--                                        <td>{{$info->last_login_at}}</td>--}}
{{--                                        <td>{{$info->last_login_ip}}</td>--}}

{{--                                    </tr>--}}
{{--                                @endforeach--}}

{{--                            @endif--}}
{{--                            </tbody>--}}
{{--                        </table>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
        <div class="card mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-12">
                        <div class="text-white mb-4 text-center">
                            <div class="card-body">
                                <a href="{{ url('create-invoice') }}" type="button" class="btn btn-primary">  POS</a>
                                <a href="{{ url('invoice-list') }}" type="button" class="btn btn-primary"> All Sell</a>
                                <a href="{{ url('customer-create') }}"  type="button" class="btn btn-primary">  New Customer</a>
                                <a href="{{ url('customers') }}"  type="button" class="btn btn-primary">  All Customer</a>
                                <a href="{{ url('new-product') }}"  type="button" class="btn btn-primary">  New Product</a>
                                <a href="{{ url('product-list') }}"  type="button" class="btn btn-primary">  All Product</a>
                                <a href="{{ url('create-table') }}"  type="button" class="btn btn-primary">  New Table</a>
                                <a  href="{{ url('table-list') }}" type="button" class="btn btn-primary">  All Table</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
