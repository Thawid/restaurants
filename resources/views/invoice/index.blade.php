@extends('layout.main')
@section('style')
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet"/>
    <style>
        /* -- quantity box -- */

        .quantity {
            display: inline-block; }

        .quantity .input-text.qty {
            width: 35px;
            height: 39px;
            padding: 0 5px;
            text-align: center;
            background-color: transparent;
            border: 1px solid #efefef;
        }

        .quantity.buttons_added {
            text-align: left;
            position: relative;
            white-space: nowrap;
            vertical-align: top; }

        .quantity.buttons_added input {
            display: inline-block;
            margin: 0;
            vertical-align: top;
            box-shadow: none;
        }

        .quantity.buttons_added .minus,
        .quantity.buttons_added .plus {
            padding: 7px 10px 8px;
            height: 39px;
            background-color: #ffffff;
            border: 1px solid #efefef;
            cursor:pointer;}

        .quantity.buttons_added .minus {
            border-right: 0; }

        .quantity.buttons_added .plus {
            border-left: 0; }

        .quantity.buttons_added .minus:hover,
        .quantity.buttons_added .plus:hover {
            background: #eeeeee; }

        .quantity input::-webkit-outer-spin-button,
        .quantity input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            -moz-appearance: none;
            margin: 0; }

        .quantity.buttons_added .minus:focus,
        .quantity.buttons_added .plus:focus {
            outline: none; }


    </style>
@endsection
@section('body')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">Add New Sell</h1>
                    <a href="{{url('invoice-list')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"> All Sell </a>
                </div>
            </div>
            <!-- page header -->
            <div class="col-md-12">
                <div class="status"></div>
            </div>

            <div class="col-md-6">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered medicine-list" id="dataTable" width="100%"
                                   cellspacing="0">
                                <thead>
                                <tr>
                                    <th> Code </th>
                                    <th>Product Name</th>
                                    <th>Price</th>
                                    <th>Add</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <form action="{{route('invoice.data')}}"  onsubmit="return (validateForm());" method="post">
                    @csrf
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="mb-4">

                                <div class="form-row align-items-center">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <select class="custom-select mr-sm-1 customer-list" id="customer_id" name="customer_id" required>
                                                <option value="0">Choose Customer...</option>
                                                @foreach($customer_list as $key=> $customer)
                                                    <option @if($key == 0) selected @endif value="{{$customer->id}}">{{$customer->customer_name}}</option>
                                                @endforeach
                                            </select>
                                            <button type="button" class="btn btn-outline-dark btn-sm" data-toggle="modal" data-target="#addCustomer"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <select class="custom-select mr-sm-2 representative-list" id="table_id" name="table_id" required>
                                            <option value="0">Choose Table...</option>
                                            @foreach($representative_list as $key=> $representative)

                                                <option @if($key ==0) selected @endif value="{{$representative->id}}">{{$representative->name}}</option>
                                            @endforeach

                                        </select>
                                    </div>

                                </div>

                            </div>

                            <table class="table thtableheader product_list" id="sell_list">

                                <tbody>
                                <tr>
                                    <th style="width: 100px">Product Name</th>
                                    <th>Code</th>
                                    <th style="width: 80px">Qty</th>
                                    <th>Price</th>
                                    <th>Total</th>
                                    <th style="width: 20px"></th>
                                </tr>
                                </tbody>
                            </table>
                            <table class="table thtablefooter">
                                <tbody>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td class="text-left">Total :</td>
                                    <td class="total_qty text-left">0</td>
                                    <input type="hidden" name="total_qty" id="total_qty">
                                    <td class="total_sum text-center">0</td>
                                    <input type="hidden" name="total_product_price" id="total_product_price">

                                </tr>
                                <tr>
                                    <td colspan="4" class="text-right">Vat ({{$vat }}%)   : </td>
                                    <td>
                                        <div class="input-group">
                                            <input class="form-control" type="number" name="vat" id="vat"   readonly>
                                        </div>
                                        <input class="form-control" type="hidden" name="vat_value" id="vat_value" value="{{ $vat }}" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="text-right">Grand Total : </td>
                                    <td class="grand_total">0</td>
                                    <input type="hidden" name="grand_total" id="grand_total">
                                </tr>
                                <tr>
                                    <td></td>
                                    <td  class="text-right">Discount  </td>
                                    <td>
                                        <div class="input-group">
                                            <input class="form-control" type="number" step="any" name="discount" id="discount"  onchange="claculate_discount_percent(this)">
                                            <div class="input-group-append">
                                                <span class="input-group-text">%</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td></td>
                                    <td>
                                        <div class="input-group">
                                            <input class="form-control" type="number" step="any" value="" name="discount_amount" id="discount_amount" onblur="claculate_discount(this)">
                                            <div class="input-group-append">
                                                <span class="input-group-text"> ৳ </span>
                                            </div>
                                        </div>
                                    </td>

                                </tr>
                                <tr>
                                    <td colspan="4" class="text-right">Paid  </td>
                                    <td><input class="form-control" type="number" step="any" name="paid" id="paid" value="0" onkeyup="paidAmount(this)"> </td>
                                </tr>

                                <tr>
                                    <td></td>
                                    <td class="text-left">Change : </td>
                                    <td class="change text-left">
                                        0
                                        <input type="hidden" name="payable" id="change">
                                    </td>

                                    <td class="text-right">Payable:  </td>
                                    <td id="paybale" class="due text-left">
                                        0
                                    </td>
                                    <input type="hidden" name="receiveable" id="due">


                                </tr>
                                </tbody>
                            </table>
                            <button type="submit" id="submit" class="btn bg-success btn-sm"> Submit </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addCustomer">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addCustomer">Fill the information </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="add_customer">
                        @csrf
                        <p>
                            <small><i>* Every information given by the star must be provided correctly </i></small>
                        </p>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="customerName">Customer Name * </label>
                                <input type="text" class="form-control" id="customer_name" name="customer_name" placeholder="Jon Dow" />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="customerMobile">Mobile No  </label>
                                <input type="number" class="form-control" id="phone" name="phone" placeholder="01676000000" />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="customerMobile"> E-Mail </label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com" />
                            </div>
                            <div class="form-group col-md-6">
                                <div class="form-group">
                                    <label for="customerAddress">Address </label>
                                    <textarea class="form-control" id="address" name="address" placeholder="Customer address here"  rows="1"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bg-danger" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn bg-success">Submit </button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{asset('assets/js/select2.min.js')}}"></script>


    <script>
        $(function () {

            var table = $('.medicine-list').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('medicine.list') }}",
                columns: [
                    {
                        data: 'mergeColumn',
                        name: 'mergeColumn',
                        searchable: false,
                        sortable: false,
                        visible: true,
                        className: 'product_size'
                    },
                    {data: 'name', name: 'name', className: 'product_name'},
                    {data: 'price', name: 'price', className: 'product_price'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: 'packing', name: 'packing', searchable: true, sortable: true, visible: false},

                ]
            });

        });
        $(document).ready(function () {
            $('.customer-list').select2();
            $('.representative-list').select2();
        });
    </script>

    <script>

        var product_name = '';
        var product_size = '';
        var product_price = '';
        var amount = '';



        function get_product_details(current) {

            product_name = $(current).parent().parent().find(".product_name").text();
            product_size = $(current).parent().parent().find(".product_size").text();
            product_price = $(current).parent().parent().find(".product_price").text();

        }



        function add_product_to_chart(product_id, context, product_id) {

            get_product_details(context);
            product_price = product_price.replace(/[^0-9]/gi, '');
            var price_input = '<input style="width:45px" type="text" name="product_price[]" value="' + product_price + '" id="pprice_'+product_id+'"   class="chart_product_price" readonly>';

            var row = '<tr><td class="single_name">' + product_name + '</td><td class="single_size">' + product_size + '</td>\n' +
                '<input name="product_id[]" type="hidden" value="' + product_id + '" class="">\n' +
                /*'<td><input style="width:45px" name="product_qty[]" onkeyup="claculate_qty(this)" type="number" class="form-control single_qty qty-input product_qty"  value="1"  required></td>\n' +*/
                /*'<td> <div class="quantity buttons_added"><input type="button" value="-" class="minus" product="'+product_id+'"><input type="text" step="1" min="0" max="" name="product_qty[]" value="1" title="Qty" class="input-text qty text single_qty qty-input product_qty" id="pid_'+product_id+'" size="4" pattern="" inputmode="" onkeyup="claculate_qty(this)" required><input type="button" value="+" product="'+product_id+'" class="plus"> </div></td>\n' +*/
                '<td><div class="quantity buttons_added"><input class="minus" type="button" id="desc" product="'+product_id+'" value="-" onclick="decrement(this)"><input  id="pid_'+product_id+'" type="text"  value="1" name="product_qty[]" min="1" max=""  class="input-text qty text single_qty qty-input product_qty" onkeyup="claculate_qty(this)"><input class="plus" id="inc"  type="button" product="'+product_id+'" value="+" onclick="increment(this)"></div></td>\n' +
                '<td  class="current_product_price">' + price_input + '</td>\n' +
                '<td class="sub_total" id="subtotalprice_'+product_id+'">0 tk</td>\n' +
                '<td><a href="javascript:void(0);"><i onclick="back_product_to_list(' + product_id + ',this,' + product_id + ')" class="far fa-window-close"></i></a></td></tr>';
            $(".product_list").append(row);
            $(context).parent().parent().hide();
            calculate_total();
            payableAmount();
        }


        //return product to productlist from chart
        function back_product_to_list(product_id, context, product_id) {
            $(".product_" + product_id).show();
            $(context).parent().parent().parent().remove();
            calculate_total();
            payableAmount();

        }

        //calculate quantity and price of product
        function claculate_qty(context) {
            var price = $(context).parent().parent().find(".current_product_price").text();

            var qty = context.value;
            price = price.substring(1);
            $(context).parent().parent().find(".sub_total").text(qty * price);
            calculate_total();
            //paidAmount(context);
            payableAmount();

        }

        //calculate total sub of product
        function calculate_total() {
            var sum = 0;
            var total_qty = 0;
            var qty_chart = 0;
            var change = 0;
            var due = 0;
            var discount = 0;
            var discountAmount = 0;
            var discountPercentage = 0;

            $(".chart_product_price").each(function () {
                var val = $.trim($(this).val());
                qty_chart = $(this).parent().parent().find('.product_qty').val();
                var qty_price = (qty_chart > 0) ? qty_chart * val : 0;
                $(this).parent().parent().find('.sub_total').text(qty_price);

                sum += qty_price;
                total_qty = parseFloat(qty_chart) + parseFloat(total_qty);
            });


            $('.total_sum').text(sum);
            $('.total_qty').text(parseFloat(total_qty).toFixed(2));
            $('#total_product_price').val(sum);
            $('#total_qty').val(parseFloat(total_qty).toFixed(2));

            var  vat_value = $("#vat_value").val() ? $("#vat_value").val() : 0;
            var  getDiscount = $("#discount").val() ? $("#discount").val() : 0;

            var calculate_vat = (( sum * vat_value)/100);
            var grandTotal = parseFloat(sum) + parseFloat(calculate_vat);

            if(getDiscount > 0) {
                discount = (grandTotal * getDiscount) / 100;
            }

            $("#grand_total").val(grandTotal);
            $(".grand_total").text(grandTotal);
            $("#vat").val(calculate_vat);
        }

        function paidAmount(e){
            const paid = parseFloat(e.value);
            const grand_total = document.getElementById('grand_total').value;
            const dis_count = document.getElementById('discount_amount').value;
            const findal_discount = dis_count ? parseFloat(dis_count) : 0;
            const final_total = grand_total ? parseFloat(grand_total) : 0;
            let due = (final_total - (paid + findal_discount)) ;
            document.getElementById('paybale').innerText = due.toFixed(2);
            document.getElementById('due').value = due.toFixed(2);
            let change = 0;
            if(paid > final_total){
                change = parseFloat(paid) - parseFloat(final_total) ;
                due = 0;
            }

            $("#change").val(change.toFixed(2));
            $(".change").text(change.toFixed(2));

            $(".due").text(due.toFixed(2));
            $("#due").val(due.toFixed(2));
        }

        function claculate_discount_percent(e){
            //console.log(e.value)
            const tanvirTotal = document.getElementById('grand_total').value;
            const tdisCountAmount = e.value;
            const tanvirDiscountPercent = (tanvirTotal * tdisCountAmount) / 100;
            document.getElementById('discount_amount').value = Math.round(tanvirDiscountPercent);
            //console.log(tanvirDiscountPercent);
            payableAmount();
        }

        function claculate_discount(e){
            //console.log(e.value)
            const tanvirTotal = document.getElementById('grand_total').value;
            const total = tanvirTotal ? parseFloat(tanvirTotal)  : '0';
            const tdisCountAmount = e.value;
            const tanvirDiscountPercent = ((tdisCountAmount/total)*100);
            const disPercent = tanvirDiscountPercent ? parseFloat(tanvirDiscountPercent) : '0';
            document.getElementById('discount').value = disPercent;
            //console.log(tanvirDiscountPercent);
            payableAmount();
        }
        function payableAmount(){
            const totalPayable = document.getElementById('grand_total').value;
            const payAble = totalPayable ? parseFloat(totalPayable) : '0';
            const paid = document.getElementById('paid').value;
            const paidAmount = paid ? parseFloat(paid) : '0';
            const discount = document.getElementById('discount_amount').value;
            const discountAmount = discount ? parseFloat(discount) : '0';
            let due = payAble - ( paidAmount + discountAmount);


            $(".due").text(due.toFixed(2));
            $("#due").val(due.toFixed(2));

        }

        function validateForm(){
            //e.preventDefault();

            var customer_error = "Customer not selected"
            var product_error = "Product not added to list"
            var representative_error = "Select Table"
            var customer_id = $("#customer_id").val();
            var table_id = $("#table_id").val();
            var error = false;
            if (customer_id==0){
                //$(".status").html(customer_error);
                alert(customer_error)
                return false;
            }
            if(table_id == 0){
                alert(representative_error);
                return false;
            }

            if ( $(".current_product_price").length ){
                $(".single_qty").each(function(){
                    if(!this.value>0){
                        var product_name =  $(this).parent().parent().find(".single_name").text();
                        var product_qty_error = product_name+" quantity not found";
                        //$(".status").html(product_qty_error);
                        alert(product_qty_error)
                        return false;
                    }
                });
            }else{
                //$(".status").html(product_error);
                alert(product_error)
                return false;
            }
            if (!error){
                return true;
            }
        }

    </script>
    <script>
        function increment(e) {
            let incProductId = e.getAttribute('product');
            var value = parseInt(document.getElementById(`pid_${incProductId}`).value, 10);
            value = isNaN(value) ? 0 : value;
            value++;
            document.getElementById(`pid_${incProductId}`).value = value;
            qt_subtotal(incProductId)
            calculate_total();
            payableAmount();

        }
        function decrement(e) {
            let decProductId = e.getAttribute('product');
            var value = parseInt(document.getElementById(`pid_${decProductId}`).value, 10);
            value = isNaN(value) ? 0 : value;
            value--;
            if(value > 0){
                document.getElementById(`pid_${decProductId}`).value = value;
            }else {
                alert(`Negative value or zero does not allowed` )
                document.getElementById(`pid_${decProductId}`).value = 1;
            }

            qt_subtotal(decProductId)
            calculate_total();
            payableAmount();

        }
    </script>
    <script>
        function qt_subtotal(pid){
            //console.log(pid);
            const productQty = document.getElementById('pid_'+pid).value;
            //console.log(productQty);
            const productPrice = document.getElementById('pprice_'+pid).value;
            const subTotal = document.getElementById('subtotalprice_'+pid);
            subTotal.innerText = parseFloat(productQty) * parseFloat(productPrice)+' Tk';

        }
    </script>
    <script>
        $("#add_customer").submit(function(e){

            e.preventDefault();
            toastr.options = {
                "closeButton": true,
                "newestOnTop": true,
                "positionClass": "toast-top-right"
            };
            let customer_name = $("#customer_name").val();
            let phone = $("#phone").val();
            let email = $("#email").val();
            let address = $("#address").val();
            //$(".customer").val(customer_name);
            //console.log(customer_name);
            let _token = $("input[name= _token]").val();
            $.ajax({
                url :"{{route('add.customer')}}",
                type:"POST",
                data:{
                    customer_name:customer_name,
                    phone:phone,
                    email:email,
                    address:address,
                    _token:_token,
                },
                success:function (response) {
                    if(response){
                        toastr.success(response.message);
                        $("#customer_id").append(new Option(customer_name,response.customer_id));
                        $("#customer_id").val(response.customer_id).selected;
                        $("#addCustomer").modal('toggle');
                        $("#add_customer")[0].reset();
                    }

                },
                error: function(err){
                    toastr.error("Something went wrong !");
                    $("#addCustomer").modal('toggle');
                    $("#add_customer")[0].reset();
                }
            });
        });
    </script>
    <script>

        @if(Session::has('message'))
        toastr.success("{{Session::get('message')}}");
        @endif

        @if(count($errors) > 0)
        @foreach($errors->all() as $error)
        toastr.error("{{ $error }}");
        @endforeach
        @endif

    </script>

@endsection
