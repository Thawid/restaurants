@extends('layout.main')

@section('style')
@endsection

@section('body')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0"> All Sell </h1>
                    <a href="#" class="btn btn-danger" id="deleteSelectedRecord">Delete Selected</a>
                    <a href="{{url('create-invoice')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"> Add New Sell </a>
                </div>
            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered invoice-list" id="dataTable" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th><input type="checkbox" id="checkAll"/></th>
                                    <th>Invoice No</th>
                                    <th>Date </th>
                                    <th>Customer Name </th>
                                    <th>Total  </th>
                                    <th>Vat  </th>
                                    <th>Grand Total </th>
                                    <th>Discount </th>
                                    <th>Receiveable </th>
                                    <th>Status </th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>CK</th>
                                    <th>Invoice No</th>
                                    <th>Date </th>
                                    <th>Customer Name </th>
                                    <th>Total  </th>
                                    <th>Vat  </th>
                                    <th>Grand Total </th>
                                    <th>Discount </th>
                                    <th>Rayable </th>
                                    <th>Status </th>
                                    <th>Action</th>
                                </tr>
                                </tfoot>
                                <tbody>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->


            <!--  payment modal -->
            <div class="modal fade" id="paymentModal">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="paymentModal">Get Payment</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="update_payment">
                                @csrf
                                <input type="hidden" id="id" name="id">
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="receiveable"> Payable </label>
                                        <input type="text" class="form-control" id="receiveable" name="receiveable" disabled />
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="name"> Paid  * </label>
                                        <input type="text" class="form-control" id="paid" name="paid" placeholder="0.00" />
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn bg-danger" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn bg-success">Submit </button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end payment modal -->
        </div>
    </div>

@endsection


@section('script')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>

    <script>
        $(function () {

            var table = $('.invoice-list').DataTable({
                processing: true,
                serverSide: true,
                order: [[ 2, 'DESC' ]],
                rowId: 'sid',
                ajax: "{{ route('get.invoice.list') }}",
                columns: [

                    {data: 'delete', name: 'delete', orderable: false, searchable: false},
                    {data: 'invoice_no', name: 'sells.invoice_no'},
                    {data: 'created_at', name: 'sells.created_at'},
                    {data: 'customer_name', name: 'customers.customer_name'},
                    {data: 'total_product_price', name: 'sells.total_product_price'},
                    {data: 'vat', name: 'sells.vat'},
                    {data: 'grand_total', name: 'sells.grand_total'},
                    {data: 'discount_amount', name: 'sells.discount_amount'},
                    {data: 'receiveable', name: 'sells.receiveable'},
                    {data: 'payment_status', name: 'payment_status'},
                    {data: 'action', name: 'action', orderable: false, searchable: false },
                    {data: 'id', name: 'sell_id',searchable: false, sortable: false, visible: false},
                    {data: 'rownum', name: 'rownum', orderable: false, searchable: false,visible: false},

                ]
            });

        });
    </script>
    <script>
        function getPayment(id) {
            //console.log(id);
            $.get('payment/'+id,function(payment) {
                console.log(payment)
                $("#id").val(payment.id);
                $("#receiveable").val(payment.receiveable);
                $("#paid").val(payment.paid);
                $("#paymentModal").modal('toggle');
            });
        }
    </script>
    <script>
        $("#update_payment").submit(function(e){

            e.preventDefault();
            toastr.options = {
                "closeButton": true,
                "newestOnTop": true,
                "positionClass": "toast-top-right"
            };
            let id = $("#id").val();
            let receiveable = $("#receiveable").val();
            let paid = $("#paid").val();
            let _token = $("input[name= _token]").val();
            $.ajax({
                url :"{{route('update.payment')}}",
                type:"PUT",
                data:{
                    id:id,
                    receiveable:receiveable,
                    paid:paid,
                    _token:_token,
                },
                success:function (response) {
                    if(response){
                        toastr.success(response.message);
                        $("#paymentModal").modal('toggle');
                        $("#update_payment")[0].reset();
                        $('.invoice-list').DataTable().ajax.reload();
                        //location.reload();
                    }

                },
                error: function(err){
                    if(err){
                        toastr.error('Paid amount must be equal to payable amount');
                        $("#paymentModal").modal('toggle');
                        $("#update_payment")[0].reset();
                    }
                }
            });
        });
    </script>
    <script>
        $(function (e){
            $("#checkAll").click(function (){
                $(".checkBoxClass").prop('checked',$(this).prop('checked'));
            });

            $("#deleteSelectedRecord").click(function (e){
                e.preventDefault();

                var allIds = [];

                $("input:checkbox[name=ids]:checked").each(function (){
                    allIds.push($(this).val());
                    //console.log(allIds)
                });
                let _token = $("input[name= _token]").val();
                $.ajax({
                    url:"{{route('delete.multiple.invoice')}}",
                    type:"DELETE",
                    data:{
                        ids:allIds,
                        _token:_token
                    },
                    success:function (response){
                        if(response){
                            toastr.success(response.message);
                            $.each(allIds,function(key,val){
                                $(".sell_"+val).remove();
                                $('.invoice-list').DataTable().ajax.reload();

                            })
                        }

                    }
                });
            })

        });
    </script>
@endsection
