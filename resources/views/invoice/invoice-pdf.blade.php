<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title> Invoice </title>

<style>

    body {
        margin: 0 auto;
        font-family: 'examplefont', sans-serif;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.5;
        color: #212529;
        background-color: #fff;
        padding: 0;
    }
    table {
        text-align: center;
        margin: 0;
        padding: 0;
        width: 100%;
    }
    .th-table-header, .th-table-info, .th-table-body{
        margin-bottom: 20px;
    }

    .th-table-header table tr td h1 {
        margin: 0;
        font-size: 20px;
    }
    .th-table-header table tr td h2 {
        margin: 0;
        font-size: 18px;
    }
    .th-table-header table tr td h3 {
        padding: 0;
        margin: 0;
        font-size: 16px;
    }
    .th-table-info p{
        margin: 0;
    }
    .th-table-info table tr td p{
        text-align:left !important;
    }
    .th-table-header table tr td p {
        padding: 0;
        margin: 0;
        font-size: 16px;
    }
    .th-table-body {
        width: 100%;
        text-align: center;
        border: 1px solid #ddd;
    }
    .th-table-body thead{
        border: 1px solid #ddd;
        background-color: #ddd;
    }
    .th-table-body tbody tr td{
        border: 1px solid #ddd;

    }
    .th-table-signature{
        margin-top: 40px;
    }


</style>
</head>
<body>

        <div class="th-table-header">
            <table>
                <tr>
                    <td><h1>হামদর্দ ল্যাবরেটরীজ (ওয়াক্ফ) বাংলাদেশ</h1></td>
                </tr>
                <tr>
                    <td><p>রুপায়ন ট্রেড সেন্টার (১৩-১৪ তলা ),কাজী নজরুল ইসলাম এভিনিউ-বাংলামোটর,ঢাকা-১০০০</p></td>
                </tr>
                <tr>
                    <td><h3>পরিবেশক : মনি স্টোর ফার্মেসি,হামদর্দ - হাটহাজারী শাখা চট্টগ্রাম , মোবাইল : ০১৮৫৮০৫৮৭৬০, ০১৩১৩৭৮১০৮১</h3></td>
                </tr>
                <tr>
                    <td><h2>ইনভয়েস</h2></td>
                </tr>
            </table>
        </div>
        <div class="th-table-info">
            <table>
                <tr>
                    <td style="width: 100px;"><p>তারিখ : </p></td>
                    <td><p> @if(isset($sells_item)){{bangla(date('d-m-Y',strtotime($sells_item->created_at)))}}@endif </p></td>
                    <td style="width: 80px;"><p> নাম : </p></td>
                    <td><p> @if(isset($sells_item)) {{$sells_item->customer->customer_name}} @endif </p></td>


                </tr>
                <tr>
                    <td style="width: 100px;"><p> প্রতিনিধি : </p></td>
                    <td><p>@if(isset($sells_item)) {{$sells_item->representative->name}} @endif </p></td>
                    <td style="width: 80px;"><p> ঠিকানা:</p></td>
                    <td><p> @if(isset($sells_item)) {{$sells_item->customer->address}} @endif </p></td>
                </tr>
                <tr>
                    <td style="width: 100px;"><p>মোবাইল : </p></td>
                    <td><p>@if(isset($sells_item)) {{bangla($sells_item->representative->phone)}} @endif </p></td>
                    <td style="width: 80px;"><p>মোবাইল :</p></td>
                    <td><p> @if(isset($sells_item)) {{bangla($sells_item->customer->phone)}} @endif </p></td>
                </tr>
                <tr>
                    <td style="width: 100px;"><p> ইনভয়েস : </p></td>
                    <td><p>@if(isset($sells_item))@if($sells_item->invoice_no) {{$sells_item->invoice_no}}@endif @endif </p></td>
                </tr>
            </table>
        </div>
        <div>
            <table class="th-table-body">
                <thead>
                <tr>
                    <th>ক্রম</th>
                    <th>পণ্যের নাম</th>
                    <th>সাইজ</th>
                    <th>পরিমাণ</th>
                    <th>বোনাস</th>
                    <th>দাম</th>
                    <th>মোট</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $counter = 1;
                @endphp
                @if(isset($sells_item))

                @endif
                @foreach($sells_item->invoice as $invoice)
                    <tr>
                        <td>{{$counter++}}</td>
                        <td>{{$invoice->medicine->name}}</td>
                        <td>{{$invoice->medicine->packing .' '. $invoice->medicine->size}}</td>
                        <td>{{$invoice->product_qty}}</td>
                        <td>{{$invoice->product_bonus}}</td>
                        <td>{{$invoice->product_price}}</td>
                        <td>{{$invoice->product_qty * $invoice->product_price}}</td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>মোট : @if(isset($sells_item)) {{ $invoice->sell->total_qty }} @endif</th>

                    <th> মোট : @if(isset($sells_item)) {{ $invoice->sell->total_bonus }} @endif</th>
                    <th></th>
                    <th>সর্বমোট : @if(isset($sells_item)) {{ $invoice->sell->total_product_price }} @endif</th>
                </tr>
                </tfoot>
            </table>
        </div>
        <div class="th-table-signature">
            <table>
                <tr>
                    <td>ক্রেতার স্বাক্ষর</td>
                    <td>প্রতিনিধি</td>
                    <td>অনুমোদন</td>
                </tr>

            </table>
        </div>
    </body>


