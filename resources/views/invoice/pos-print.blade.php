


<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> {{ $store_info->company_name }}</title>
</head>
<body>
<style>
    /*@import url('https://fonts.googleapis.com/css2?family=Yanone+Kaffeesatz:wght@300;400;500&display=swap');*/
    body{
        font-family: 'Yanone Kaffeesatz', sans-serif;
    }
    #invoice {
        width: 320px;
        margin: 0 auto;
        font-size: 10px;
        background: #fff;
        padding: 20px 12px;
    }
    #invoice p {
        display: block;
        margin: 5px 0;
    }
    .logo-wrapper{
        height: 100px;
        margin: auto;
        text-align: center;
    }
    .logo-wrapper img {
        height: auto;
        width: 100%;
    }

    .invoice-title-wrapper {
        overflow-x: hidden;
        text-align: center;
    }
    .invoice-title-wrapper> p:first-child{
        font-size:16px;
    }


    .invoice-product-table-wrapper {
        overflow-x: hidden;
    }

    .invoice-calculation-area {
        overflow-x: hidden;
    }
    .invoice-calculation-area > div{
        display: flex;
        justify-content: flex-end;

    }
    .invoice-details-wrapper>div:first-child{
        display: flex;
        justify-content: space-between;
    }
    #invoice .separator{
        overflow: hidden;
    }
    #invoice .separator2{
        margin:0;
        line-height:0
    }
    thead th {
        font-style: normal;
        font-weight: 400;
        font-size: 16px;
    }

    tbody {
        font-size: 15px;
    }

    table th, table td {
        padding: 2px !important;
        font-size: 10px;
        text-align: left;
    }

    .flex-item{
        display: flex;
        justify-content: flex-end
    }
    .flex-item > div:first-child{
        margin-right:20px;
        text-align: right
    }
    .flex-item > div:last-child{
        text-align: right;
        margin-right:5px;
        min-width: 18%;
    }
    .bar-code-wrapper span{
        width: 100% !important;
        height: 100% !important;
        margin-top:6px;
    }
    .bar-code-wrapper img {
        height: 100%;
        width:100%
    }
    #invoice .notes > p:nth-child(1),
    #invoice .notes > p:nth-child(5),
    #invoice .notes > p:nth-child(6){
        text-align: center
    }
    #invoice .notes > p:first-child{
        font-size: 15px;
        margin-top:8px
    }
    #invoice .notes > p:nth-child(5){
        margin-top: 10px;
    }
    @media  print {
        * {
            font-size:16px!important;
        }

        div.invoice-product-table-wrapper th  {
            font-weight: 900;
        }
        td,th {padding: 5px 0;}
        .hidden-print {
            display: none !important;
        }
        @page  { margin: 0; } body { margin: 0.5cm; margin-bottom:1.6cm; }
    }

</style>
<div id="invoice" class="printable-area" >
    <div class="invoice-title-wrapper">
        <h4 style="margin-bottom: 2px;"> {{ $store_info->company_name }}</h4>
        <p> {{ strip_tags($store_info->address) }}</p>
        <p> {{ ($store_info->phone) ? 'PH : ' . $store_info->phone : ''}}</p>
        <p> {{  ($store_info->vat_reg) ? 'VAT Reg : '.$store_info->vat_reg : ''  }}</p>
        <p> {{ $store_info->email ?? '' }}</p>
        <p>{{ $store_info->web_site ?? '' }}</p>
        <p>*********************************************************</p>
    </div>
    <div class="invoice-details-wrapper">
        <div>
            <span>Invoice No: {{ $sells_item->invoice_no }}</span><br>
            <span>Date : @if(isset($sells_item)){{date('d-m-Y',strtotime($sells_item->created_at))}}@endif</span>
        </div>
        <p>
            <span> {{ isset($sells_item->customer->customer_name)?$sells_item->customer->customer_name:" " }}  </span>
            <br>
            <span> {{ isset($sells_item->customer->phone)? $sells_item->customer->phone : "N/A" }}  </span>
        </p>
        <p>{{ isset($sells_item->representative->name)?$sells_item->representative->name:" " }} </p>
    </div>
    <p class="separator">***************************************************************************</p>
    <div class="invoice-product-table-wrapper">
        <table class="table table-striped table-bordered table-hover bg-white" style="width: 100%">
            <thead>
            <tr>
                <th style="text-align: left">Food Name</th>
                <th style="text-align: center">Price</th>
                <th style="text-align: center">Qty</th>
                <th style="text-align: right">Total</th>
            </tr>
            </thead>
            <tbody>
            @if (!empty($sells_item))
                @foreach ($sells_item->invoice as $invoice)
                    <tr>
                        <td style="text-align: left">{{$invoice->medicine->name}}</td>
                        <td style="text-align: center">{{ number_format($invoice->product_price,2) }}</td>
                        <td style="text-align: center">{{ number_format($invoice->product_qty,1) }}</td>
                        <td style="text-align: right">{{ number_format($invoice->product_qty * $invoice->product_price,2) }}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
        <p>------------------------------------------------------------</p>
    </div>
    <div class="invoice-calculation-area">
        <div class="flex-item">
            <div>
                <p>Total : </p>
                <p> Vat ({{$vat }}%) : </p>
            </div>
            <div>
                <p> {{ isset($sells_item) ? number_format($invoice->sell->total_product_price,2) : '' }} /=</p>
                <p> {{ isset($sells_item) ? number_format($sells_item->vat,2) : '' }} /=</p>
            </div>
        </div>
        <p class="separator2">-------------------------------------------------------------------------------------------------------------------------</p>
        <div class="flex-item">
            <div>
                <p> Grand Total : </p>
                <p> Discount ({{ isset($sells_item) ? number_format($sells_item->discount) .'%' : '' }} )  </p>
                <p> Paid : </p>
                <p> Payable : </p>
            </div>
            <div>
                <p> {{ isset($sells_item) ? number_format($sells_item->grand_total,2) : '' }} /=</p>
                <p> {{ isset($sells_item) ? number_format($sells_item->discount_amount,2) : '' }} /=</p>
                <p> {{ isset($sells_item) ? number_format($sells_item->paid,2) : '' }} /=</p>
                @if($sells_item->paid == ($sells_item->grand_total - $sells_item->discount_amount))
                <p> Paid </p>
                @else
                    <p> {{ isset($sells_item) ? number_format($sells_item->receiveable,2) : '' }} /=</p>
                @endif
            </div>
        </div>
    </div>
    <div class="notes">
        <p>{{ nl2br(strip_tags(str_replace("&nbsp;", '', $store_info->footer_text)))  ?? '' }}</p>
        <p style="text-align: center">Powered By WinnerDevs, 01676966260</p>
        <p class="separator2">-------------------------------------------------------------------------------------------------------------------------</p>
    </div>
</div>
</body>
<script>
    window.print();
    setTimeout(function(){
        window.history.back();
    },300);
</script>
</html>
