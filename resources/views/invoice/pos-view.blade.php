@extends('layout.main')

@section('style')
    <link href="{{asset('assets/solaiman-lipi/font.css')}}" rel="stylesheet" type="text/css" />
    <style>
        .main-content {
            background: #fff;
            padding: 30px 0;
        }

        #invoice {
            width: 320px;
            margin: 0 auto;
            font-size: 16px;
            overflow: auto;
            background: #f8f9fb;
            padding: 20px 12px;
        }

        #invoice p {
            display: block;
        }

        .invoice-title-wrapper {
            overflow-x: hidden;
            text-align: center;
        }

        .invoice-product-table-wrapper table {
            font-size: 25px;
        }

        .invoice-product-table-wrapper {
            overflow-x: hidden;
        }

        thead th {
            font-style: normal;
            font-weight: 400;
            font-size: 16px;
        }

        tbody {
            font-size: 15px;
        }

        table th,
        table td {
            padding: 2px !important;
            font-size: 20;
        }

        .invoice-calculation-area {
            overflow-x: hidden;
        }

        .invoice-calculation-area>div {
            display: flex;
            justify-content: space-between;

        }

        /* .logo-wrapper{
            width: 300px;
            height: 300px;
        } */
        .logo-wrapper img {
            height: auto;
            width: 100%;
        }

        .bar-code-wrapper img {
            height: 50px;
            width: 120px;
        }

        #button-area i {
            font-size: 18px;
        }


        @media print {
            * {
                font-size: 21px;

            }

            td,
            th {
                padding: 5px 0;
            }

            .hidden-print {
                display: none !important;
            }

            @page {
                margin: 0;
            }

            body {
                margin: 0.5cm;
                margin-bottom: 1.6cm;
            }
        }
    </style>
@endsection


@section('body')
    <div id="button-area" class="text-right pt-4">
        <a href="{{route('pos.print',['id'=>$sells_item->id])}}" target="_blank" class="btn btn-outline-primary mr-3">Print
            <i class="fa fa-print"></i>
        </a>
    </div>

    <div id="invoice" class="printable-area">
        <div class="invoice-title-wrapper">
            <h3> {{ $store_info->company_name }}</h3>
            <p> {{ strip_tags($store_info->address) }}</p>
            <p> {{ ($store_info->phone) ? 'PH : ' . $store_info->phone : ''}}</p>
            <p> {{  ($store_info->vat_reg) ? 'VAT Reg : '.$store_info->vat_reg : ''  }}</p>
            <p> {{ $store_info->email ?? '' }}</p>
            <p>{{ $store_info->web_site ?? '' }}</p>
            <p>********************************************************</p>
        </div>

        <div class="invoice-details-wrapper">
            <span>Invoice No: {{ $sells_item->invoice_no }}</span><br>
            <span>Date  : @if(isset($sells_item)){{ date('d-m-Y',strtotime($sells_item->created_at)) }}@endif</span>
            <p>{{ isset($sells_item->customer->customer_name)?$sells_item->customer->customer_name:" " }} <br> {{ isset($sells_item->customer->phone)? $sells_item->customer->phone :"N/A" }}</p>
            <p>{{ isset($sells_item->representative->name)?$sells_item->representative->name:" " }} </p>

        </div>

        <div class="invoice-product-table-wrapper">
            <table class="table table-striped table-bordered table-hover bg-white">
                <thead>
                <tr>
                    <th>Product Name</th>
                    <th>Qty</th>
                    <th>Price</th>
                    <th>Total</th>
                </tr>
                </thead>
                <tbody>
                @if (!empty($sells_item))
                    @foreach ($sells_item->invoice as $invoice)
                        <tr>
                            <td>{{$invoice->medicine->name}}</td>
                            <td>{{ number_format($invoice->product_qty,1) }}</td>
                            <td>{{ number_format($invoice->product_price,2) }}</td>
                            <td>{{ number_format($invoice->product_qty * $invoice->product_price,2) }}</td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            </table>
            <p>************************************************************************</p>
        </div>

        <div class="invoice-calculation-area">
            <div>
                <div>
                    <p>Total : </p>
                    <p> Vat ({{ $vat }}%) : </p>
                    <p>Grand Total : </p>
                    <p> Paid : </p>
                    <p> Discount ({{ isset($sells_item) ? number_format($sells_item->discount) .'%' : '' }} )  </p>
                    <p> Payable : </p>

                </div>

                <div>
                    {{--{{ dd($sells_item) }}--}}
                    <p> {{ isset($sells_item) ? number_format($invoice->sell->total_product_price,2) : '' }} /=</p>
                    <p> {{ isset($sells_item) ? number_format($sells_item->vat,2) : '' }} /=</p>
                    <p> {{ isset($sells_item) ? number_format($sells_item->grand_total,2) : '' }} /=</p>
                    <p> {{ isset($sells_item) ? number_format($sells_item->paid,2) : '' }} /=</p>
                    <p> {{ isset($sells_item) ? number_format($sells_item->discount_amount,2) : '' }} /=</p>
                    @if($sells_item->grand_total == ($sells_item->paid + $sells_item->discount_amount))
                        <p> Paid </p>
                    @else
                        <p> {{ isset($sells_item) ? number_format($sells_item->receiveable,2) : '' }} /=</p>
                    @endif

                </div>
            </div>
            <p>*********************************************************</p>
            <div class="logo-bar_code-wrapper">
                <div class="bar-code-wrapper" style="width: 100%">
                </div>
            </div>
            <p class="text-center mt-3">{{ strip_tags($store_info->footer_text) ?? '' }}</p>
        </div>
    </div>


@endsection


@section('script')
    <script src="{{asset('assets/js/jquery.printarea.js')}}"></script>
    <script>
        $(function() {
            $("#print_btn").on('click', function() {

                var mode = 'iframe'; //popup
                var close = mode == "popup";
                var options = {
                    mode: mode,
                    popClose: close
                };
                $("#printable-area").printArea(options);
            });
        });
    </script>
@endsection
