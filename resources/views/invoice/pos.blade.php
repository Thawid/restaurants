@extends('layout.main')
@section('style')
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet"/>
    <style>
        /* -- quantity box -- */

        .quantity {
            display: inline-block; }

        .quantity .input-text.qty {
            width: 35px;
            height: 39px;
            padding: 0 5px;
            text-align: center;
            background-color: transparent;
            border: 1px solid #efefef;
        }

        .quantity.buttons_added {
            text-align: left;
            position: relative;
            white-space: nowrap;
            vertical-align: top; }

        .quantity.buttons_added input {
            display: inline-block;
            margin: 0;
            vertical-align: top;
            box-shadow: none;
        }

        .quantity.buttons_added .minus,
        .quantity.buttons_added .plus {
            padding: 7px 10px 8px;
            height: 39px;
            background-color: #ffffff;
            border: 1px solid #efefef;
            cursor:pointer;}

        .quantity.buttons_added .minus {
            border-right: 0; }

        .quantity.buttons_added .plus {
            border-left: 0; }

        .quantity.buttons_added .minus:hover,
        .quantity.buttons_added .plus:hover {
            background: #eeeeee; }

        .quantity input::-webkit-outer-spin-button,
        .quantity input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            -moz-appearance: none;
            margin: 0; }

        .quantity.buttons_added .minus:focus,
        .quantity.buttons_added .plus:focus {
            outline: none; }


    </style>
@endsection
@section('body')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">Add New Sell</h1>
                    <a href="{{url('invoice-list')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"> All Sell </a>
                </div>
            </div>
            <!-- page header -->
            <div class="col-md-12">
                <div class="status"></div>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <label for="employee_name">Product Name *</label>
                    <select class="livesearch form-control p-3" name="livesearch"></select>

                </div>
            </div>
            <div class="col-md-12">
                <form action="{{route('invoice.data')}}"  onsubmit="return (validateForm());" method="post">
                    @csrf
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="mb-4">

                                <div class="form-row align-items-center">
                                    <div class="col-md-6">
                                        <div class="input-group">
                                            <select class="custom-select mr-sm-1 customer-list" id="customer_id" name="customer_id" required>
                                                <option value="0">Choose Customer...</option>
                                                @foreach($customer_list as $key=> $customer)
                                                    <option @if($key == 0) selected @endif value="{{$customer->id}}">{{$customer->customer_name}}</option>
                                                @endforeach
                                            </select>
                                            <button type="button" class="btn btn-outline-dark btn-sm" data-toggle="modal" data-target="#addCustomer"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <select class="custom-select mr-sm-2 representative-list" id="table_id" name="table_id" required>
                                            <option value="0">Choose Table...</option>
                                            @foreach($representative_list as $key=> $representative)

                                                <option @if($key ==0) selected @endif value="{{$representative->id}}">{{$representative->name}}</option>
                                            @endforeach

                                        </select>
                                    </div>

                                </div>

                            </div>

                            <table class="table thtableheader product_list" id="sell_list">

                                <thead>
                                    <tr>
                                        <th style="width: 100px">Product Name</th>
                                        <th>Code</th>
                                        <!--                                    <th style="width: 5px">পরিমাণ</th>-->
                                        <th style="width: 80px">Qty</th>
                                        <th>Price</th>
                                        <th>Total</th>

                                        <th style="width: 20px"></th>
                                    </tr>
                                </thead>
                                <tbody id="selected_product">
                                </tbody>
                            </table>
                            <table class="table thtablefooter">
                                <tbody>
                                <tr>
                                    <td colspan="2" style="text-align: center;">Total :</td>
                                    <td style="text-align:left;" class="total_qty">0</td>
                                    <input type="hidden" name="total_qty" id="total_qty">
                                    <td colspan="2" style="text-align: center;" class="total_sum">0</td>
                                    <input type="hidden" name="total_product_price" id="total_product_price">

                                </tr>
                                <tr>
                                    <td colspan="4" class="text-right">Vat ({{$vat }}%)   : </td>
                                    <td>
                                        <div class="input-group">
                                            <input class="form-control" type="number" name="vat" id="vat"   readonly>
                                        </div>
                                        <input class="form-control" type="hidden" name="vat_value" id="vat_value" value="{{ $vat }}" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="text-right">Grand Total : </td>
                                    <td class="grand_total">0</td>
                                    <input type="hidden" name="grand_total" id="grand_total">
                                </tr>
                                <tr>
                                    <td colspan="4" class="text-right">Paid : </td>
                                    <td><input class="form-control" type="number" name="paid" id="paid" value="0" onkeyup="claculate_qty(this)"> </td>
                                </tr>
                                <tr>
                                    <td colspan="4" class="text-right">Discount : </td>
                                    <td><input class="form-control" type="number" step="any" name="discount" id="discount" value="0" onkeyup="claculate_qty(this)"> </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td colspan="" class="text-left">Change : </td>
                                    <td class="change text-left">0</td>
                                    <input type="hidden" name="payable" id="change">
                                    <td colspan="" class="text-left">Payable : </td>
                                    <td class="due text-left">0</td>
                                    <input type="hidden" name="receiveable" id="due">

                                </tr>
                                </tbody>
                            </table>
                            <button type="submit" id="submit" class="btn bg-success btn-sm"> Submit </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addCustomer">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="addCustomer">Fill the information </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="add_customer">
                        @csrf
                        <p>
                            <small><i>* Every information given by the star must be provided correctly </i></small>
                        </p>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="customerName">Customer Name * </label>
                                <input type="text" class="form-control" id="customer_name" name="customer_name" placeholder="Jon Dow" />
                            </div>
                            <div class="form-group col-md-6">
                                <label for="customerMobile">Mobile No * </label>
                                <input type="number" class="form-control" id="phone" name="phone" placeholder="01676000000" />
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="customerMobile"> E-Mail </label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com" />
                            </div>
                            <div class="form-group col-md-6">
                                <div class="form-group">
                                    <label for="customerAddress">Address </label>
                                    <textarea class="form-control" id="address" name="address" placeholder="Customer address here"  rows="1"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bg-danger" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn bg-success">Submit </button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{asset('assets/js/select2.min.js')}}"></script>


    <script>
        $(document).ready(function () {
            $('.customer-list').select2();
            $('.representative-list').select2();
        });
    </script>
    <script>
        $(document).ready(function() {
            $(".livesearch").select2({
                ajax: {
                    url: "{{ route('get.products') }}",
                    dataType: 'json',
                    delay: 250,

                    processResults: function(data, params) {
                        console.log(data);
                        params.page = params.page || 1;

                        let products = [];
                        data.forEach(element => {
                            let item = {
                                id: element.id,
                                text: element.name,
                            }
                            products.push(item);
                        })
                        return {
                            // results: data,
                            results: products,
                        };
                    },
                },
                placeholder: 'Search for a Product',
                minimumInputLength: 2,
                // templateResult: formatRepo,
                // templateSelection: formatRepoSelection
            }).on('select2:closing', function(e) {
                var product_id = e.target.value;
                getProductData(product_id);

            });
            //$('.livesearch').select2('open');
        })
    </script>
    <script>
        function getProductData(id) {

            $(".livesearch").empty();
            var vm = this;
            var product_id = id;
            //console.log(product_id)
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'GET',
                url: "{{ route('add.products') }}",
                data: {
                    product_id : id
                },

                success: function(data) {

                    var html =
                        `<tr class="deleted_product${data[0].id}" id="deleted_product${data[0].id}">
                               <td style="width: 20%" class="single_name">${data[0].name}</td><td class="single_size">${data[0].packing}</td>\n' +
                '<input name="product_id[]" id="product_id${data[0].id}" type="hidden" value="${data[0].id}" class="">\n' +
                '<td style="width: 20%"> <div class="quantity buttons_added"><input type="button" value="-" class="minus" product="${data[0].id}"><input type="number" step="1" min="1" max="" name="product_qty[]" value="1" title="Qty" class="input-text qty text single_qty qty-input product_qty" id="pid_${data[0].id}" size="4" pattern="" inputmode="" required><input type="button" value="+" product="${data[0].id}" class="plus"> </div></td>\n' +
                '<td style="width: 15%"  class="current_product_price"><input  type="text" name="product_price[]" value="${data[0].price}" id="pprice_${data[0].id}"   class="chart_product_price" readonly></td>\n' +
                '<td style="width: 15%" class="sub_total" id="subtotalprice_${data[0].id}"> 0 tk </td>\n' +
                '<td style="width: 5%"> <div onclick="removeProduct(this)" class="btn btn-danger remove-product">x</div></td>
                            </tr>`;
                    var product = document.querySelector('#product_id' + data[0].id);
                    if (!product) {
                        $('#selected_product').append(html);
                        calculate_total();

                    } else {
                        alert('Product Already Added')
                        //$('.livesearch').select2('open');
                    }
                },
                error: function(err) {
                    console.log(err.responseJSON);
                }
            });

        }

        /*
         * Remove Product Rows
         *
         */

        function removeProduct(em) {
            $(em).closest('tr').remove();
            calculate_total();
        }
    </script>
    <script>

        var product_name = '';
        var product_size = '';
        var product_price = '';
        var amount = '';

        //calculate quantity and price of product
        function claculate_qty(context) {
            var price = $(context).parent().parent().find(".current_product_price").text();

            var qty = context.value;
            price = price.substring(1);
            $(context).parent().parent().find(".sub_total").text(`${qty * price} Tk`);
            //$(context).parent().parent().find(".sub_total").text(qty * price + `Tk`);
            calculate_total();
        }

        //calculate total sub of product
        function calculate_total() {
            var sum = 0;
            var total_qty = 0;
            var qty_chart = 0;
            var change = 0;
            var due = 0;

            $(".chart_product_price").each(function () {
                var val = $.trim($(this).val());
                qty_chart = $(this).parent().parent().find('.product_qty').val();
                var qty_price = (qty_chart > 0) ? qty_chart * val : 0;
                $(this).parent().parent().find('.sub_total').text(`${qty_price} Tk`);

                sum += qty_price;
                total_qty = parseInt(qty_chart) + parseInt(total_qty);
            });


            $('.total_sum').text(sum);
            $('.total_qty').text(total_qty);
            $('#total_product_price').val(sum);
            $('#total_qty').val(total_qty);

            var  vat_value = $("#vat_value").val() ? $("#vat_value").val() : 0;
            var  discount = $("#discount").val() ? $("#discount").val() : 0;
            var calculate_vat = (( sum * vat_value)/100);
            var grandTotal = parseFloat(sum) + parseFloat(calculate_vat)
            $("#grand_total").val(grandTotal);
            $(".grand_total").text(grandTotal);

            var paid = $("#paid").val() ? $("#paid").val() : 0;

            if(paid > 0){
                if(paid > grandTotal){
                    change = parseFloat(paid) - parseFloat(grandTotal) ;
                    due = 0;

                }
            }
            if(paid >= 0){
                due = parseFloat(grandTotal) - ( parseFloat(paid) + parseFloat(discount));
            }
            $("#vat").val(calculate_vat);
            $("#change").val(change);
            $(".change").text(change);
            $("#due").val(due);
            $(".due").text(due);
        }

        function validateForm(){
            //e.preventDefault();

            var customer_error = "Customer not selected"
            var product_error = "Product not added to list"
            var representative_error = "Select Table"
            var customer_id = $("#customer_id").val();
            var table_id = $("#table_id").val();
            var error = false;
            if (customer_id==0){
                //$(".status").html(customer_error);
                alert(customer_error)
                return false;
            }
            if(table_id == 0){
                alert(representative_error);
                return false;
            }

            if ( $(".current_product_price").length ){
                $(".single_qty").each(function(){
                    if(!this.value>0){
                        var product_name =  $(this).parent().parent().find(".single_name").text();
                        var product_qty_error = product_name+" quantity not found";
                        //$(".status").html(product_qty_error);
                        alert(product_qty_error)
                        return false;
                    }
                });
            }else{
                //$(".status").html(product_error);
                alert(product_error)
                return false;
            }
            if (!error){
                return true;
            }
        }

    </script>
    <script>
        function wcqib_refresh_quantity_increments() {
            jQuery("div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)").each(function(a, b) {
                var c = jQuery(b);
                c.addClass("buttons_added"), c.children().first().before('<input type="button" value="-" class="minus" />'), c.children().last().after('<input type="button" value="+" class="plus" />')
            })
        }
        String.prototype.getDecimals || (String.prototype.getDecimals = function() {
            var a = this,
                b = ("" + a).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
            return b ? Math.max(0, (b[1] ? b[1].length : 0) - (b[2] ? +b[2] : 0)) : 0
        }), jQuery(document).ready(function() {
            wcqib_refresh_quantity_increments()
        }), jQuery(document).on("updated_wc_div", function() {
            wcqib_refresh_quantity_increments()
        }), jQuery(document).on("click", ".plus, .minus", function() {
            const productID =  this.getAttribute('product');
            var a = jQuery(this).closest(".quantity").find(".qty"),
                b = parseFloat(a.val()),
                c = parseFloat(a.attr("max")),
                d = parseFloat(a.attr("min")),
                e = a.attr("step");
            b && "" !== b && "NaN" !== b || (b = 0), "" !== c && "NaN" !== c || (c = ""), "" !== d && "NaN" !== d || (d = 0), "any" !== e && "" !== e && void 0 !== e && "NaN" !== parseFloat(e) || (e = 1), jQuery(this).is(".plus") ? c && b >= c ? a.val(c) : a.val((b + parseFloat(e)).toFixed(e.getDecimals())) : d && b <= d ? a.val(d) : b > 0 && a.val((b - parseFloat(e)).toFixed(e.getDecimals())), a.trigger("change")
            calculate_total();
            qt_subtotal(productID)


        });

        function qt_subtotal(pid){
            console.log('hello');
            console.log(pid);
            const productQty = document.getElementById('pid_'+pid).value;
            console.log(productQty);
            const productPrice = document.getElementById('pprice_'+pid).value;
            const subTotal = document.getElementById('subtotalprice_'+pid);
            subTotal.innerText = parseFloat(productQty) * parseFloat(productPrice)+' Tk';


        }
    </script>
    <script>
        $("#add_customer").submit(function(e){

            e.preventDefault();
            toastr.options = {
                "closeButton": true,
                "newestOnTop": true,
                "positionClass": "toast-top-right"
            };
            let customer_name = $("#customer_name").val();
            let phone = $("#phone").val();
            let email = $("#email").val();
            let address = $("#address").val();
            //$(".customer").val(customer_name);
            console.log(customer_name);
            let _token = $("input[name= _token]").val();
            $.ajax({
                url :"{{route('add.customer')}}",
                type:"POST",
                data:{
                    customer_name:customer_name,
                    phone:phone,
                    email:email,
                    address:address,
                    _token:_token,
                },
                success:function (response) {
                    if(response){
                        toastr.success(response.message);
                        $("#customer_id").append(new Option(customer_name,response.customer_id));
                        $("#customer_id").val(response.customer_id).selected;
                        $("#addCustomer").modal('toggle');
                        $("#add_customer")[0].reset();
                    }

                },
                error: function(err){
                    toastr.error("Something went wrong !");
                    $("#addCustomer").modal('toggle');
                    $("#add_customer")[0].reset();
                }
            });
        });
    </script>

    <script>

        @if(Session::has('message'))
        toastr.success("{{Session::get('message')}}");
        @endif

        @if(count($errors) > 0)
        @foreach($errors->all() as $error)
        toastr.error("{{ $error }}");
        @endforeach
        @endif

    </script>

@endsection
