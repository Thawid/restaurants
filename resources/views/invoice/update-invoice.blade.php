@extends('layout.main')

@section('style')
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet"/>
    <style>
        /*.product_list th{
            font-size: 13px !important;
        }
        .product_list  td{
            font-size: 13px !important;
        }*/

        /* -- quantity box -- */

        .quantity {
            display: inline-block; }

        .quantity .input-text.qty {
            width: 35px;
            height: 39px;
            padding: 0 5px;
            text-align: center;
            background-color: transparent;
            border: 1px solid #efefef;
        }

        .quantity.buttons_added {
            text-align: left;
            position: relative;
            white-space: nowrap;
            vertical-align: top; }

        .quantity.buttons_added input {
            display: inline-block;
            margin: 0;
            vertical-align: top;
            box-shadow: none;
        }

        .quantity.buttons_added .minus,
        .quantity.buttons_added .plus {
            padding: 7px 10px 8px;
            height: 39px;
            background-color: #ffffff;
            border: 1px solid #efefef;
            cursor:pointer;}

        .quantity.buttons_added .minus {
            border-right: 0; }

        .quantity.buttons_added .plus {
            border-left: 0; }

        .quantity.buttons_added .minus:hover,
        .quantity.buttons_added .plus:hover {
            background: #eeeeee; }

        .quantity input::-webkit-outer-spin-button,
        .quantity input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            -moz-appearance: none;
            margin: 0; }

        .quantity.buttons_added .minus:focus,
        .quantity.buttons_added .plus:focus {
            outline: none; }


    </style>
@endsection

@section('body')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0"> Update Sell</h1>
                    <a href="{{url('create-invoice')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"> Add New Sell </a>
                </div>
            </div>
            <!-- page header -->
            <div class="col-md-6">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered medicine-list" id="dataTable" width="100%"
                                   cellspacing="0">
                                <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Code</th>
                                    <th>Price</th>
                                    <th>Add</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card mb-4">
                    <div class="card-body">
                        <form action="{{route('update.invoice.data',$sells_item->id)}}" method="post">
                            @csrf
                            <div class="form-row align-items-center">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select required name="customer_id" class="custom-select mr-sm-2 customer-list" data-live-search="true" id="customer_id" data-live-search-style="begins" title="Select customer...">
                                            @foreach($customers as $customer)
                                                <option value="{{$customer->id}}" @if($sells_item->customer_id == $customer->id)  selected @endif >{{$customer->customer_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select required name="representative_id" class="custom-select mr-sm-2 representative-list" data-live-search="true" id="representative_id" data-live-search-style="begins" title="Select customer...">
                                            @foreach($representative_list as $representative)
                                                <option value="{{$representative->id}}" @if($sells_item->representative_id == $representative->id)  selected @endif >{{$representative->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered update-invoice product_list"  width="100%" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th style="width: 100px">Product Name</th>
                                        <th>Code</th>
                                        <th style="width: 80px">Qty</th>
                                        <th>Price</th>
                                        <th>Total</th>
                                        <th>Action </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{--{{ $sells_item->product_id }}--}}
                                    @php
                                        $counter = 1;
                                        $product_qty = 0;
                                        $total_price =0;
                                        $total_bonus = 0;
                                    @endphp
                                    @if(isset($sells_item))

                                    @endif
                                    @foreach($sells_item->invoice as $invoice)

                                        @php
                                            $total_price = $total_price+$invoice->product_price*$invoice->product_qty;
                                            $product_qty = $product_qty+$invoice->product_qty;
                                            $total_bonus = $total_bonus+$invoice->product_bonus;
                                        @endphp
                                        <tr>
                                            <td>{{$invoice->medicine->name}}</td>
                                            <td>{{$invoice->medicine->packing .' '. $invoice->medicine->size}}</td>
{{--                                            <td><input style="width:50px" type="number" name="product_qty[]" onkeyup="claculate_qty(this)" class="form-control single_qty qty-input product_qty" value="{{$invoice->product_qty}}" required></td>--}}
{{--                                            <td><div class="quantity buttons_added"><input type="button" value="-" class="minus" product="{{$invoice->product_id}}"><input style="width:50px" type="number" step="1" min="1" max="" name="product_qty[]" value="{{$invoice->product_qty}}" title="Qty" class="input-text qty text single_qty qty-input product_qty" id="pid_{{$invoice->product_id}}" size="4" pattern="" inputmode="" required><input type="button" value="+" product="{{$invoice->product_id}}" class="plus"> </div></td>--}}
                                            <td><div class="quantity buttons_added"><input class="minus" type="button" id="desc" product="{{$invoice->product_id}}" value="-" onclick="decrement(this)"><input  id="pid_{{$invoice->product_id}}" type="text"  value="{{$invoice->product_qty}}" name="product_qty[]" min="1" max=""  class="input-text qty text single_qty qty-input product_qty" onkeyup="claculate_qty(this)"><input class="plus" id="inc"  type="button" product="{{$invoice->product_id}}" value="+" onclick="increment(this)"></div></td>
                                            <td><input style="width:100px" type="text" name="product_price[]" onkeyup="calculate_total()" class="form-control chart_product_price" value="{{$invoice->product_price}}" id="pprice_{{$invoice->product_id}}" readonly></td>
                                            <td class="sub_total" id="subtotalprice_{{ $invoice->product_id}}"> {{ $invoice->product_qty *  $invoice->product_price}}</td>

                                            <td><button type="button"  class="ibtnDel btn btn-md btn-danger btn-sm"><i class="far fa-window-close"></i></button></td>
                                            <input type="hidden" name="id[]" value="{{$invoice->id}}">
                                            <input type="hidden" name="sell_id" value="{{$invoice->sell_id}}">
                                            <input type="hidden" name="product_id[]" value="{{$invoice->product_id}}">
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>

                                <table class="table thtablefooter">
                                    <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>Total  </td>
                                        <td><input  type="text" name="total_qty" class="form-control total_qty" id="total_qty" value="{{$product_qty}}" readonly></td>

                                        <td><input type="text" class="form-control total_sum" id="total_product_price" name="total_product_price" value="{{$total_price}}" readonly></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" class="text-right">Vat ({{$vat }}%): </td>
                                        <td>
                                            <div class="input-group">
                                                <input class="form-control" type="number" name="vat" id="vat" value="{{ $sells_item->vat }}"  readonly>
                                                <input class="form-control" type="hidden" name="vat_value" id="vat_value" value="{{ $vat }}" readonly>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" class="text-right">Grand Total : </td>
                                        <td class="grand_total" value="{{ $sells_item->grand_total }}">0</td>
                                        <input type="hidden" name="grand_total" id="grand_total" value="{{ $sells_item->grand_total }}">
                                    </tr>
<!--                                    <tr>
                                        <td colspan="5" class="text-right">Discount : </td>
                                        <td>
                                            <div class="input-group">
                                                <input class="form-control" type="number" step="any" name="discount" id="discount" value="{{ $sells_item->discount }}" onkeyup="claculate_qty(this)" >
                                                <input type="hidden" step="any" name="discount_amount" id="discount_amount">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">%</span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>-->
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td  class="text-right">Discount  </td>
                                        <td>
                                            <div class="input-group">
                                                <input class="form-control" type="number" step="any" name="discount" id="discount" value="{{ $sells_item->discount }}"  onchange="claculate_discount_percent(this)">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">%</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td></td>
                                        <td>
                                            <div class="input-group">
                                                <input class="form-control" type="number" step="any" value="{{ $sells_item->discount_amount }}" name="discount_amount" id="discount_amount" onblur="claculate_discount(this)">
                                                <div class="input-group-append">
                                                    <span class="input-group-text"> ৳ </span>
                                                </div>
                                            </div>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td colspan="5" class="text-right">Paid : </td>
                                        <td><input class="form-control" type="number" name="paid" id="paid" onkeyup="claculate_qty(this)" value="{{ $sells_item->paid }}"> </td>
                                    </tr>

                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td  class="text-left">Change: </td>
                                        <td class="change text-left" value="{{ $sells_item->payable }}">0</td>
                                        <input type="hidden" name="payable" id="change">
                                        <td colspan="" class="text-left">Payable : </td>
                                        <td class="due text-left" value="{{ $sells_item->receiveable }}">0</td>
                                        <input type="hidden" name="receiveable" id="due">

                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->
        </div>
    </div>
@endsection


@section('script')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{asset('assets/js/select2.min.js')}}"></script>
    <script>
        $(function () {

            var table = $('.medicine-list').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('medicine.list') }}",
                columns: [
                    {data: 'name', name: 'name', className: 'product_name'},
                    {
                        data: 'mergeColumn',
                        name: 'mergeColumn',
                        searchable: false,
                        sortable: false,
                        visible: true,
                        className: 'product_size'
                    },
                    {data: 'price', name: 'price', className: 'product_price'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: 'packing', name: 'packing', searchable: true, sortable: true, visible: false},

                ]
            });

        });
        $(document).ready(function () {
            $('.customer-list').select2();
            $('.representative-list').select2();
        });
    </script>
    <script>
        calculate_total();
        var product_name = '';
        var product_size = '';
        var product_price = '';
        var amount = '';

        var product_qty = [];
        var product_price = [];
        var product_bonus = [];

        $(document).ready(function () {
            $('.customer-list').select2();
        });

        /*Delete product temporary*/
        $("table.update-invoice tbody").on("click", ".ibtnDel", function(event) {
            rowindex = $(this).closest('tr').index();
            product_price.splice(rowindex, 1);
            product_qty.splice(rowindex, 1);
            product_bonus.splice(rowindex, 1);
            $(this).closest("tr").remove();
            calculate_total();
        });

        function get_product_details(current) {

            product_name = $(current).parent().parent().find(".product_name").text();
            product_size = $(current).parent().parent().find(".product_size").text();
            product_price = $(current).parent().parent().find(".product_price").text();

        }

        function add_product_to_chart(product_id, context, product_id) {

            get_product_details(context);
            product_price = product_price.replace(/[^0-9]/gi, '');
            var price_input = '<input style="width:100px" type="text" name="product_price[]" value="' + product_price + '" id="pprice_'+product_id+'" onkeyup="calculate_total()" class="form-control chart_product_price" readonly>';

            var row = '<tr><td class="single_name">' + product_name + '</td><td class="single_size">' + product_size + '</td>\n' +
                '<input name="product_id[]" type="hidden" value="' + product_id + '" class="">\n' +
                /*'<td><input style="width:50px" name="product_qty[]" onkeyup="claculate_qty(this)" value="1" type="number" class="form-control single_qty qty-input product_qty" placeholder="" required></td>\n' +*/
                /*'<td><div class="quantity buttons_added"><input type="button" value="-" class="minus" product="'+product_id+'"><input style="width:50px" type="number" step="1" min="1" max="" name="product_qty[]" value="1" title="Qty" class="input-text qty text single_qty qty-input product_qty" id="pid_'+product_id+'" size="2" pattern="" inputmode="" required><input type="button" value="+" product="'+product_id+'" class="plus"> </div></td>\n' +*/
                '<td><div class="quantity buttons_added"><input class="minus" type="button" id="desc" product="'+product_id+'" value="-" onclick="decrement(this)"><input  id="pid_'+product_id+'" type="text"  value="1" name="product_qty[]" min="1" max=""  class="input-text qty text single_qty qty-input product_qty" onkeyup="claculate_qty(this)"><input class="plus" id="inc"  type="button" product="'+product_id+'" value="+" onclick="increment(this)"></td>\n' +
                '<td  class="current_product_price">' + price_input + '</td>\n' +
                '<td class="sub_total" id="subtotalprice_'+product_id+'">0 tk</td>\n' +
                '<td><a class="btn btn-md btn-danger btn-sm" href="javascript:void(0);"><i onclick="back_product_to_list(' + product_id + ',this,' + product_id + ')" class="far fa-window-close"></i></a></td></tr>';
            $(".product_list").append(row);
            $(context).parent().parent().hide();
            calculate_total();
        }

        //return product to productlist from chart
        function back_product_to_list(product_id, context, product_id) {
            $(".product_" + product_id).show();
            $(context).parent().parent().parent().remove();
            calculate_total();
        }

        //calculate quantity and price of product
        function claculate_qty(context) {
            var price = $(context).parent().parent().find(".current_product_price").text();

            var qty = context.value;
            price = price.substring(1);
            $(context).parent().parent().find(".sub_total").text(qty * price);
            calculate_total();
        }

        //calculate total sub of product
        function calculate_total() {
            var sum = 0;
            var total_qty = 0;
            var total_bonus = 0;
            var qty_chart = 0;
            var qty_bonus = 0;
            var change = 0;
            var due = 0;
            var paid = 0;

            $(".chart_product_price").each(function () {
                var val = $.trim($(this).val());
                qty_chart = $(this).parent().parent().find('.product_qty').val();
                qty_bonus = $(this).parent().parent().find('.product_bonus').val();
                var qty_price = (qty_chart > 0) ? qty_chart * val : 0;
                $(this).parent().parent().find('.sub_total').text(qty_price);
                $(this).parent().parent().find('.total_bonus').text(qty_bonus);
                sum += qty_price;
                total_qty = parseFloat(qty_chart) + parseFloat(total_qty);
                total_bonus = parseFloat(qty_bonus) + parseFloat(total_bonus);
            });

            $('.total_sum').text(sum);
            $('.total_qty').text(parseFloat(total_qty).toFixed(2));
            $('#total_product_price').val(sum);
            $('#total_qty').val(total_qty);

            var  vat_value = $("#vat_value").val() ? $("#vat_value").val() : 0;
            var  getDiscount = $("#discount").val() ? $("#discount").val() : 0;
            var calculate_vat = (( sum * vat_value)/100);
            var grandTotal = parseFloat(sum) + parseFloat(calculate_vat)
            /*var discount = (grandTotal*getDiscount)/100;
            $("#discount_amount").val(parseFloat(discount).toFixed(2));*/
            $("#grand_total").val(grandTotal);
            $(".grand_total").text(grandTotal);

            paid = $("#paid").val() ? $("#paid").val() : 0;

            if(paid > 0){
                if(paid > grandTotal){
                    change = parseFloat(paid) - parseFloat(grandTotal) ;
                    due = 0;

                }

            }

            if(paid < grandTotal){
                due = parseFloat(grandTotal) - ( parseFloat(paid) + parseFloat(discount));
            }
            $("#vat").val(calculate_vat);
            $("#change").val(parseFloat(change).toFixed(2));
            $(".change").text(parseFloat(change).toFixed(2));
            /*$("#due").val(parseFloat(due).toFixed(2));
            $(".due").text(parseFloat(due).toFixed(2));*/
            payableAmount();
        }
        function claculate_discount_percent(e){
            //console.log(e.value)
            const tanvirTotal = document.getElementById('grand_total').value;
            const tdisCountAmount = parseFloat(e.value);
            const tanvirDiscountPercent = (tanvirTotal * tdisCountAmount) / 100;
            document.getElementById('discount_amount').value = Math.round(tanvirDiscountPercent);
            //console.log(tanvirDiscountPercent);
            payableAmount();
        }

        function claculate_discount(e){
            //console.log(e.value)
            const tanvirTotal = document.getElementById('grand_total').value;
            const total = tanvirTotal ? parseFloat(tanvirTotal)  : '0';
            const tdisCountAmount = e.value;
            const tanvirDiscountPercent = ((tdisCountAmount/total)*100);
            const disPercent = tanvirDiscountPercent ? parseFloat(tanvirDiscountPercent) : '0';
            document.getElementById('discount').value = disPercent;
            //console.log(tanvirDiscountPercent);
            payableAmount();
        }
        function payableAmount(){
            const totalPayable = document.getElementById('grand_total').value;
            const payAble = totalPayable ? parseFloat(totalPayable) : '0';
            const paid = document.getElementById('paid').value;
            const paidAmount = paid ? parseFloat(paid) : '0';
            const discount = document.getElementById('discount_amount').value;
            const discountAmount = discount ? parseFloat(discount) : '0';
            let due = payAble - ( paidAmount + discountAmount);


            $(".due").text(due.toFixed(2));
            $("#due").val(due.toFixed(2));

        }
    </script>
    <script>
        /*function wcqib_refresh_quantity_increments() {
            jQuery("div.quantity:not(.buttons_added), td.quantity:not(.buttons_added)").each(function(a, b) {
                var c = jQuery(b);
                c.addClass("buttons_added"), c.children().first().before('<input type="button" value="-" class="minus" />'), c.children().last().after('<input type="button" value="+" class="plus" />')
            })
        }
        String.prototype.getDecimals || (String.prototype.getDecimals = function() {
            var a = this,
                b = ("" + a).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
            return b ? Math.max(0, (b[1] ? b[1].length : 0) - (b[2] ? +b[2] : 0)) : 0
        }), jQuery(document).ready(function() {
            wcqib_refresh_quantity_increments()
        }), jQuery(document).on("updated_wc_div", function() {
            wcqib_refresh_quantity_increments()
        }), jQuery(document).on("click", ".plus, .minus", function() {
            const productID =  this.getAttribute('product');
            var a = jQuery(this).closest(".quantity").find(".qty"),
                b = parseFloat(a.val()),
                c = parseFloat(a.attr("max")),
                d = parseFloat(a.attr("min")),
                e = a.attr("step");
            b && "" !== b && "NaN" !== b || (b = 0), "" !== c && "NaN" !== c || (c = ""), "" !== d && "NaN" !== d || (d = 0), "any" !== e && "" !== e && void 0 !== e && "NaN" !== parseFloat(e) || (e = 1), jQuery(this).is(".plus") ? c && b >= c ? a.val(c) : a.val((b + parseFloat(e)).toFixed(e.getDecimals())) : d && b <= d ? a.val(d) : b > 0 && a.val((b - parseFloat(e)).toFixed(e.getDecimals())), a.trigger("change")

            qt_subtotal(productID)
            calculate_total();

        });*/
        function increment(e) {
            let incProductId = e.getAttribute('product');
            var value = parseInt(document.getElementById(`pid_${incProductId}`).value, 10);
            value = isNaN(value) ? 0 : value;
            value++;
            document.getElementById(`pid_${incProductId}`).value = value;
            qt_subtotal(incProductId)
            calculate_total();

        }
        function decrement(e) {
            let decProductId = e.getAttribute('product');
            var value = parseInt(document.getElementById(`pid_${decProductId}`).value, 10);
            value = isNaN(value) ? 0 : value;
            value--;
            if(value > 0){
                document.getElementById(`pid_${decProductId}`).value = value;
            }else {
                alert(`Negative value or zero does not allowed` )
                document.getElementById(`pid_${decProductId}`).value = 1;
            }

            qt_subtotal(decProductId)
            calculate_total();

        }
        function qt_subtotal(pid){
            console.log('hello');
            console.log(pid);
            const productQty = document.getElementById('pid_'+pid).value;
            console.log(productQty);
            const productPrice = document.getElementById('pprice_'+pid).value;
            const subTotal = document.getElementById('subtotalprice_'+pid);
            subTotal.innerText = parseInt(productQty) * parseInt(productPrice)+' Tk';

        }
    </script>
    <script>

        @if(Session::has('message'))
        toastr.success("{{Session::get('message')}}");
        @endif

        @if(count($errors) > 0)
        @foreach($errors->all() as $error)
        toastr.error("{{ $error }}");
        @endforeach
        @endif

    </script>

@endsection
