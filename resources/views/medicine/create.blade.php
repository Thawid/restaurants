@extends('layout.main')
@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0"> Add New Product </h1>
                    <a href="{{url('product-list')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="bi bi-people-fill"></i> Product List </a>
                </div>
            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <p>
                            <small><i>* Every information given by the star must be provided correctly </i></small>
                        </p>
                        <form  action="{{route('save.medicine')}}" method="post">
                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="medicineName"> Product Name * </label>
                                    <input type="text" class="form-control" id="medicineName" name="name" placeholder="Rice" />
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="medicinePacking">Product Code * </label>
                                    <input type="text" class="form-control" id="medicinePacking" name="packing" placeholder="1101" />
                                </div>

                                <div class="form-group col-md-6">
                                    <label for="medicinePrice">Price * </label>
                                    <input type="number" class="form-control" name="price" id="medicinePrice" placeholder="100" />
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="medicinePrice">Category * </label>
                                    <select name="category" id="" class="form-control">
                                        <option value="">Select Category</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}"> {{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <button type="submit" class="btn bg-success"> Submit </button>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->
        </div>
    </div>
@endsection

@section('script')
    <script>

        @if(Session::has('success'))
           toastr.success("{{Session::get('success')}}");
        @endif

        @if(count($errors) > 0)
            @foreach($errors->all() as $error)
             toastr.error("{{ $error }}");
            @endforeach
        @endif

    </script>
@endsection
