@extends('layout.main')

@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">Product List</h1>
                    <a href="{{url('new-product')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                            class="bi bi-people-fill"></i> Add Product </a>
                </div>
            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered medicine-list" id="dataTable" width="100%"
                                   cellspacing="0">
                                <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Product Name </th>
                                    <th>Category </th>
                                    <th>Code</th>
                                    <th>Price </th>
                                    <th> Action</th>
                                </tr>
                                </thead>
                                <tfoot>
                                <tr>
                                    <th>SL</th>
                                    <th>Product Name </th>
                                    <th>Category </th>
                                    <th>Code</th>
                                    <th>Price </th>
                                    <th> Action</th>
                                </tr>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->

            <!--  customer Modal Edit -->
            <div class="modal fade" id="medicineModalEdit" tabindex="-1" role="dialog"
                 aria-labelledby="medicineModalEdit" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="medicineModalEdit"> Update Information </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="update_medicine">
                                @csrf
                                <input type="hidden" id="id" name="id">
                                <div class="form-row">
                                    <div class="form-group col-md-3">
                                        <label for="name">Product Name * </label>
                                        <input type="text" class="form-control" id="name" name="name"
                                                required/>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="packing">Code * </label>
                                        <input type="text" class="form-control" name="packing" id="packing"
                                                required/>
                                    </div>

                                    <div class="form-group col-md-3">
                                        <label for="price">Price * </label>
                                        <input type="number" class="form-control" name="price" id="price"
                                               placeholder="১৬৭" required/>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="">Category </label>
                                        <select name="category_name" id="category_name" class="form-control">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        </select>
<!--                                        <input type="text" class="form-control" name="category_name" id="category_name"/>-->
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn bg-danger" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn bg-success">Submit</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <!-- End customer Modal Edit -->
            <!--  customer Modal Delete -->
            <div class="modal fade" id="medicinModalDelete" tabindex="-1" role="dialog"
                 aria-labelledby="medicinModalDelete" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="medicinModalDelete">তথ্যটি মুছ</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            আপনি কি এই তথ্যটি মুছতে চান?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn bg-warning" data-dismiss="modal">না</button>
                            <button type="button" class="btn bg-info">হ্যাঁ</button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end customer Modal Delete -->
        </div>
    </div>

@endsection

@section('script')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>

    <script>
        $(function () {

            var table = $('.medicine-list').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('get.medicine.list') }}",
                columns: [
                    {data: 'rownum', name: 'rownum', orderable: false, searchable: false},
                    {data: 'name', name: 'name'},
                    {data: 'category_name', name: 'category_name'},
                    {data: 'packing', name: 'packing'},
                    {data: 'product_price', name: 'product_price'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: 'id', name: 'id', searchable: false, sortable: false, visible: false},
                ]
            });

        });
    </script>
    <script>
        function editMedicine(id) {
            $.get('medicine/' + id, function (product) {
                $("#id").val(product.id);
                $("#name").val(product.name);
                $("#packing").val(product.packing);
                $("#price").val(product.price);
                $( "#category_name" ).val(product.cat_id).selected;
                $("#medicineModalEdit").modal('toggle');
            });
        }

        $("#update_medicine").submit(function (e) {

            e.preventDefault();
            toastr.options = {
                "closeButton": true,
                "newestOnTop": true,
                "positionClass": "toast-top-right"
            };
            let id = $("#id").val();
            let name = $("#name").val();
            let packing = $("#packing").val();
            let price = $("#price").val();
            let category_name = $("#category_name").val();
            let _token = $("input[name= _token]").val();
            $.ajax({
                url: "{{route('update.medicine')}}",
                type: "PUT",
                data: {
                    id: id,
                    name: name,
                    packing: packing,
                    price: price,
                    category_name: category_name,
                    _token: _token,
                },
                success: function (response) {
                    if (response) {
                        toastr.success(response.message);
                        $("#medicineModalEdit").modal('toggle');
                        $("#update_medicine")[0].reset();
                        $('.medicine-list').DataTable().ajax.reload();

                    }

                },
                error: function (err) {
                    toastr.error("Something went wrong !");
                    $("#medicineModalEdit").modal('toggle');
                    $("#update_medicine")[0].reset();
                }
            });
        });

        function deleteMedicine(id) {
            if (confirm('Do you realy want to delete this record?')) {
                $.ajax({
                    url: 'delete-medicine/' + id,
                    type: 'get',
                    data: {
                        _token: $("input[name=_token]").val()
                    },
                    success: function (response) {
                        toastr.success(response.message);
                        $('.medicine-list').DataTable().ajax.reload();
                    },
                    error: function (err) {
                        toastr.error("Data can't be deleted !");

                    }
                });
            }
        }
    </script>

@endsection
