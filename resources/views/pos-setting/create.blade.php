@extends('layout.main')
@section('body')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0"> Invoice Setting  </h1>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success" id="alert-success">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> {{ $message }}
                    </div>
                @endif
                @if($errors->any())
                    <div class="alert alert-danger" id="alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>Warning!</strong> {{ $error }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif

            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <p>
                            <small><i>* Every information given by the star must be provided correctly </i></small>
                        </p>
                        <form  action="{{route('update.pos.settings')}}" method="post">
                            @csrf
                            <input type="hidden" name="id" value="{{ $data->id ?? null }}">
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="companyName"> Store Name * </label>
                                    <input type="text" class="form-control" id="companyName" name="company_name" placeholder="My Store"  value="{{ $data->company_name ?? '' }}" />
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="phoneNumber">Mobile No * </label>
                                    <input type="text" class="form-control" id="phoneNumber" name="phone" placeholder="01676000000"  value="{{ $data->phone ?? '' }}" />
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="web_site">Web site  </label>
                                    <input type="text" class="form-control" id="web_site" name="web_site" placeholder="www.yourshop.com" value="{{  $data->web_site ?? '' }}"/>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="email"> E-Mail </label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="name@example.com" value="{{  $data->email ?? '' }}"/>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="email"> VAT Reg </label>
                                    <input type="text" class="form-control" id="vat_reg" name="vat_reg" placeholder="003154136-0503" value="{{  $data->vat_reg ?? '' }}"/>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="address">Address *</label>
                                    <textarea class="form-control" id="address" name="address"  rows="1">{{  $data->address ?? '' }}</textarea>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="footer_text">Footer Text *</label>
                                    <textarea class="form-control" id="footer_text" name="footer_text"  rows="1">{{  $data->footer_text ?? '' }}</textarea>
                                </div>
                            </div>
                            <button type="submit" id="submit" class="btn bg-success btn-sm">Submit </button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->
        </div>
    </div>

@endsection

@section('script')
    <script src="https://cdn.ckeditor.com/ckeditor5/30.0.0/classic/ckeditor.js"></script>
    <script>

        $("#alert-success").fadeTo(2000, 500).slideUp(500, function(){
            $("#alert-success").slideUp(500);
        });

        $("#alert-danger").fadeTo(2000, 500).slideUp(500, function(){
            $("#alert-danger").slideUp(500);
        });

    </script>
    <script>
        ClassicEditor
            .create( document.querySelector( '#address' ) )
            .catch( error => {
                console.error( error );
            } );
        ClassicEditor
            .create( document.querySelector( '#footer_text' ) )
            .catch( error => {
                console.error( error );
            } );

    </script>
@endsection
