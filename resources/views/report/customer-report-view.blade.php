<div id="printableArea">

    <div class="th-table-header">
        <table>
            <tr>
                <td><h1>{{ $company_info->company_name ?? '' }}</h1></td>
            </tr>
            <tr>
                <td><p>{{ $company_info->address ?? '' }}</p></td>
            </tr>
            <tr>
                <td>{{ $company_info->email ?? '' }}</td>
            </tr>
            <tr>
                <td>{{ $company_info->phone ?? '' }}</td>
            </tr>
            <tr>
                <td>{{ $company_info->web_site ?? '' }}</td>
            </tr>

        </table>
    </div>
    <div class="th-table-info">
        <table>
            <tr>

                <td style="width: 90px;"><p>Customer : </p></td>
                <td><p>@if(isset($customer)) {{$customer->customer_name}} @endif  </p></td>
                <td style="width: 80px;"><p>Date : </p></td>
                <td><p>@php $date = date('d-m-Y'); echo $date;  @endphp </p></td>

            </tr>
            <tr>

                <td><p>Mobile No :</p></td>
                <td><p> @if(isset($customer)) {{ $customer->phone }} @endif </p></td>
                <td style="width: 80px;"><p> Address:</p></td>
                <td><p> @if(isset($customer)) {{$customer->address}} @endif  </p></td>
            </tr>

        </table>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered" id='medicineTable'>
            <thead>
            <tr>
                <th>SL</th>
                <th> Product Name</th>
                <th>Code </th>
                <th>Qty</th>
                <th>Price</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            @php
                $count = 1;
            @endphp
            @if(isset($customer_report))
                @foreach($customer_report as $report)
                    <tr>
                        <td>{{ $count++ }}</td>
                        <td>{{ $report->name }}</td>
                        <td>{{ $report->packing }}</td>
                        <td>{{ $report->qty }}</td>
                        <td>{{ $report->price }}</td>
                        <td>{{ $report->qty * $report->price }}</td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>

</div>
<div class="from-group">
    <button class="btn btn-primary mb-2" onclick="printDiv('printableArea')">Print
    </button>
</div>

