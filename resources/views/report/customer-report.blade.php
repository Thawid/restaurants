@extends('layout.main')

@section('style')
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet"/>
    <style>

        @media print {
            @page {
                margin: 20px;
            }

        }

        table {
            text-align: center;
            margin: 0;
            padding: 0;
            width: 100%;
        }

        .th-table-header, .th-table-info, .th-table-body {
            margin-bottom: 20px;
        }

        .th-table-header table tr td h1 {
            margin: 0;
            font-size: 20px;
        }

        .th-table-header table tr td h2 {
            margin: 0;
            font-size: 18px;
        }

        .th-table-header table tr td h3 {
            padding: 0;
            margin: 0;
            font-size: 16px;
        }

        .th-table-info p {
            margin: 0;
        }

        .th-table-info table tr td p {
            text-align: left;
        }
        .th-table-info table {
            text-align: center !important;
        }

        .th-table-header table tr td p {
            padding: 0;
            margin: 0;
            font-size: 16px;
        }

    </style>
@endsection


@section('body')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0">Customer Report</h1>
                    <a href="{{url('product-list')}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                            class="bi bi-people-fill"></i> Product List </a>
                </div>
            </div>
            <!-- page header -->
            <div class="col-md-12">
                <form class="form-row" method="post">
                    <meta name="csrf-token" content="{{ csrf_token() }}"/>
                    <div class="col-md-3">
                        <div class="form-group">
                            <select class="form-control customer-list" name="search" id="person_id">
                                <option value="0">Select Customer</option>
                                @foreach($customers as $customer)
                                    <option value="{{$customer->id}}">{{ $customer->customer_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="date" class="form-control" name="start_date" id="start_date">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <input type="date" class="form-control" name="end_date" id="end_date">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="from-group">
                            <input type='button' class="btn btn-primary" value='Search' id='send'>
                        </div>
                    </div>
                </form>
                <div id="result"></div>

            </div>


            <!-- end body content col-md-12 -->
        </div>
    </div>
@endsection


@section('script')
    <script src="{{ asset('assets/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{asset('assets/js/select2.min.js')}}"></script>

    <script>
        $('.customer-list').select2();

        function sendDataUsingjQuery() {

            let params = {
                "person_id": $("#person_id").val(),
                "start_date": $("#start_date").val(),
                "end_date": $("#end_date").val(),
            }
            console.log(params);
            $.ajax({
                "method": "POST",
                "url": 'get-customer-report',
                "headers": {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                "data": {
                    data: {
                        "person_id": params.person_id,
                        "start_date": params.start_date,
                        "end_date": params.end_date,
                    }
                }
            }).done(function (response) {
                $("#result").html(response);
            });
            return false;
        }

        document.getElementById("send").addEventListener("click", function () {
            sendDataUsingjQuery();
        });

        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>


@endsection
