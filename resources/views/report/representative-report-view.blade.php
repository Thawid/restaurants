<div id="printableArea">

    <div class="th-table-header">
        <table>
            <tr>
                <td><h1>{{ $company_info->company_name ?? '' }}</h1></td>
            </tr>
            <tr>
                <td><p>{{ $company_info->address ?? '' }}</p></td>
            </tr>
            <tr>
                <td>{{ $company_info->email ?? '' }}</td>
            </tr>
            <tr>
                <td>{{ $company_info->phone ?? '' }}</td>
            </tr>
            <tr>
                <td>{{ $company_info->web_site ?? '' }}</td>
            </tr>

        </table>
    </div>
    <div class="th-table-info">
        <table>
            <tr>

                <td style="width: 100px;"><p> Table : </p></td>
                <td><p>@if(isset($table)) {{$table->name}} @endif  </p></td>
                <td style="width: 80px;"><p>Date : </p></td>
                <td><p>@php $date = date('d-m-Y'); echo $date;  @endphp </p></td>

            </tr>


        </table>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered summary-report" id='summary-report'>
            <thead>
            <tr>
                <th>SL</th>
                <th> Product Name</th>
                <th>Code </th>
                <th>Qty</th>
                <th>Price</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            @php
                $count = 1;
            @endphp
            @if(isset($report_data))
                @foreach($report_data as $summary)
                    <tr>
                        <td>{{en2bnNumber($count++)}}</td>
                        <td>{{$summary->name}}</td>
                        <td>{{$summary->packing}}</td>
                        <td>{{$summary->qty}}</td>
                        <td>{{$summary->price}}</td>
                        <td>{{$summary->qty * $summary->price}}</td>
                    </tr>

                @endforeach

            @endif
            </tbody>
        </table>
    </div>

</div>
<div class="from-group">
    <button class="btn btn-primary mb-2" onclick="printDiv('printableArea')">Print
    </button>
</div>

