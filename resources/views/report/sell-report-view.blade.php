<div id="printableArea">

    <div class="th-table-header">
        <table>
            <tr>
                <td><h1>{{ $company_info->company_name ?? '' }}</h1></td>
            </tr>
            <tr>
                <td><p>{{ $company_info->address ?? '' }}</p></td>
            </tr>
            <tr>
                <td>{{ $company_info->email ?? '' }}</td>
            </tr>
            <tr>
                <td>{{ $company_info->phone ?? '' }}</td>
            </tr>
            <tr>
                <td>{{ $company_info->web_site ?? '' }}</td>
            </tr>

        </table>
    </div>

    <div class="table-responsive">
        <table class="table table-bordered" id='medicineTable'>
            <thead>
            <tr>
                <th>SL</th>
                <th>Date</th>
                <th>Invoice No </th>
                <th>Total Price </th>
                <th>Vat </th>
                <th>Grand Total </th>
                <th>Discount </th>
                <th> Payable </th>
            </tr>
            </thead>
            <tbody>
            @php
                $count = 1;
                $total_price = 0;
                $total_vat = 0;
                $grand_total = 0;
                $discount = 0;
                $paid = 0;
            @endphp
            @if(isset($sell_report))
                @foreach($sell_report as $report)
                   @php
                       $total_price += $report->total_product_price;
                       $total_vat += $report->vat;
                       $grand_total += $report->grand_total;
                       $discount += $report->discount;
                       $paid += $report->paid;
                   @endphp
                    <tr>
                        <td>{{ en2bnNumber($count++) }}</td>
                        <td>{{ $report->created_at }}</td>
                        <td>{{ $report->invoice_no }}</td>
                        <td>{{ number_format($report->total_product_price,2) }}</td>
                        <td>{{ number_format($report->vat,2) }}</td>
                        <td>{{ number_format($report->grand_total,2) }}</td>
                        <td>{{ number_format($report->discount,2) }}</td>
                        <td>{{ number_format($report->paid,2) }}</td>

                    </tr>

                @endforeach
            @endif
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="3" class="text-right"> Total  </td>
                    <td>{{ number_format($total_price,2) }}</td>
                    <td>{{ number_format($total_vat,2) }}</td>
                    <td>{{ number_format($grand_total,2) }}</td>
                    <td>{{ number_format($discount,2) }}</td>
                    <td>{{ number_format($paid,2) }}</td>

                </tr>
            </tfoot>
        </table>
    </div>

</div>
<div class="from-group">
    <button class="btn btn-primary mb-2" onclick="printDiv('printableArea')">Print
    </button>
</div>

