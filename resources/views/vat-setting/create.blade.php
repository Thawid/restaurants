@extends('layout.main')
@section('body')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 mt-lg-4 mt-4">
                <div class="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 class="h3 mb-0"> Vat Setting  </h1>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success" id="alert-success">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> {{ $message }}
                    </div>
                @endif
                @if($errors->any())
                    <div class="alert alert-danger" id="alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <strong>Warning!</strong> {{ $error }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif

            </div>
            <!-- page header -->

            <div class="col-md-12">
                <div class="card mb-4">
                    <div class="card-body">
                        <p>
                            <small><i>* Every information given by the star must be provided correctly </i></small>
                        </p>
                        <form  action="{{route('update.vat.settings')}}" method="post">
                            @csrf
                            <input type="hidden" name="id" value="{{ $data->id ?? null }}">
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="vat"> Vat(%)  *  </label>
                                    <input type="text" class="form-control" id="vat" name="vat" placeholder="7"  value="{{ $data->vat ?? '' }}" />
                                </div>

                            </div>
                            <button type="submit" id="submit" class="btn bg-success btn-sm"> Submit </button>
                        </form>
                    </div>
                </div>
            </div>
            <!-- end body content col-md-12 -->
        </div>
    </div>

@endsection

@section('script')
    <script>

        $("#alert-success").fadeTo(2000, 500).slideUp(500, function(){
            $("#alert-success").slideUp(500);
        });

        $("#alert-danger").fadeTo(2000, 500).slideUp(500, function(){
            $("#alert-danger").slideUp(500);
        });

    </script>
@endsection
