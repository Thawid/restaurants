<?php

use App\Http\Controllers;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes([
       'register' => false,
       'reset' => true,
       'verify' => true,
]);

Route::get('/home', [Controllers\HomeController::class, 'index'])->name('home');

Route::get('/customers',[Controllers\CustomerController::class,'index'])->name('customers');
Route::get('/customer-list',[Controllers\CustomerController::class,'getAllCustomer'])->name('customer.list');
Route::get('/customer-create',[Controllers\CustomerController::class,'create_customer']);
Route::post('/save-customer',[Controllers\CustomerController::class,'store'])->name('save.customer');
Route::post('/add-customer',[Controllers\CustomerController::class,'add_customer'])->name('add.customer');
Route::get('/customer/{id}',[Controllers\CustomerController::class,'get_customer_by_id']);
Route::put('/update-customer',[Controllers\CustomerController::class,'update_customer'])->name('update.customer');
Route::get('/delete-customer/{id}',[Controllers\CustomerController::class,'delete_customer'])->name('delete.customer');

Route::get('/table-list',[Controllers\RepresentativeController::class,'index']);
Route::get('/get-representative_data',[Controllers\RepresentativeController::class,'get_representative_data'])->name('get.representative.data');
Route::get('/create-table',[Controllers\RepresentativeController::class,'create_representative']);
Route::post('/store-representative',[Controllers\RepresentativeController::class,'store_representative_data'])->name('store.representative.data');
Route::get('representative/{id}',[Controllers\RepresentativeController::class,'get_representative_by_id']);
Route::put('/update-representative',[Controllers\RepresentativeController::class,'update_representative'])->name('update.representative');
Route::get('delete-representative/{id}',[Controllers\RepresentativeController::class,'delete_representative'])->name('delete.representative');


Route::get('/product-list',[Controllers\MedicineController::class,'index']);
Route::get('/get-medicine',[Controllers\MedicineController::class,'get_medicine_list'])->name('get.medicine.list');
Route::get('/new-product',[Controllers\MedicineController::class,'new_medicine']);
Route::post('/save-medicine',[Controllers\MedicineController::class,'store'])->name('save.medicine');
Route::get('/medicine/{id}',[Controllers\MedicineController::class,'get_medicine_by_id']);
Route::put('update-medicine',[Controllers\MedicineController::class,'update_medicine'])->name('update.medicine');
Route::get('/delete-medicine/{id}',[Controllers\MedicineController::class,'delete_medicine']);

Route::get('/create-invoice',[Controllers\InvoiceController::class,'index']);
Route::post('/invoice-data',[Controllers\InvoiceController::class,'save_invoice_data'])->name('invoice.data');
Route::get('/medicine-list',[Controllers\InvoiceController::class,'medicine_list'])->name('medicine.list');
Route::get('/invoice-list',[Controllers\InvoiceController::class,'invoice_list']);
Route::get('/get-invoice-list',[Controllers\InvoiceController::class,'get_invoice_list'])->name('get.invoice.list');
Route::get('/invoice-details/{id}','App\Http\Controllers\InvoiceController@invoice_view')->name('invoice.details');
Route::get('/invoice-pos-details/{id}','App\Http\Controllers\InvoiceController@invoice_pos_view')->name('invoice.pos.details');
Route::get('/pos/{id}/print', 'App\Http\Controllers\InvoiceController@print')->name('pos.print');
Route::get('/invoice-pdf/{id}',[Controllers\InvoiceController::class,'invoice_pdf_view'])->name('pdf.view');
Route::get('/invoice-print/{id}',[Controllers\InvoiceController::class,'get_invoice_print'])->name('print.view');
Route::get('/update-invoice/{id}','App\Http\Controllers\InvoiceController@invoice_update_view');
Route::post('/update-invoice-data/{id}',[Controllers\InvoiceController::class,'save_update_data'])->name('update.invoice.data');
Route::get('/payment/{id}',[Controllers\InvoiceController::class,'get_payment_by_id']);
Route::put('/update-payment',[Controllers\InvoiceController::class,'update_payment'])->name('update.payment');

Route::get('pos',[Controllers\InvoiceController::class,'pos']);
Route::get('getProductName',[Controllers\InvoiceController::class,'getProduct'])->name('get.products');
Route::get('addProduct',[Controllers\InvoiceController::class,'addProduct'])->name('add.products');
Route::delete("/delete-multiple-invoice",[Controllers\InvoiceController::class,'delete_multiple'])->name('delete.multiple.invoice');


/*Route::get('/customerreport',[Controllers\CustomerReportController::class,'index']);
Route::get('/get_data/{id}/{start_date}/{end_date}',[Controllers\CustomerReportController::class,'get_data']);*/

Route::get('/due-report',[Controllers\DueReportController::class,'index']);
Route::post('/due-report',[Controllers\DueReportController::class,'get_due_report'])->name('due.report');

Route::get('/summary-report',[Controllers\SummaryReportController::class,'index']);
Route::post('/summary-report',[Controllers\SummaryReportController::class,'get_summary_report'])->name('summary.report');


Route::get('/customer-report',[Controllers\CustomerReportController::class,'customer_report']);
Route::post('/get-customer-report',[Controllers\CustomerReportController::class,'get_customer_report']);

Route::get('/sell-report',[Controllers\SellReportController::class,'sell_report']);
Route::post('/get-sell-report',[Controllers\SellReportController::class,'get_sell_report']);

Route::get('/table-report',[Controllers\RepresentativeReportController::class,'index']);
Route::post('/get-representative-report',[Controllers\RepresentativeReportController::class,'get_representative_report']);

Route::get('/category-list',[Controllers\CategoryController::class,'index']);
Route::get('/get_data',[Controllers\CategoryController::class,'get_category_data'])->name('category.data');

Route::get('/create-category',[Controllers\CategoryController::class,'create_category']);
Route::post('/store-category',[Controllers\CategoryController::class,'store_category'])->name('store.category');

Route::get('category/{id}',[Controllers\CategoryController::class,'get_category_by_id']);
Route::put('/update-category',[Controllers\CategoryController::class,'update_category'])->name('update.category');
Route::get('delete-category/{id}',[Controllers\CategoryController::class,'delete_category'])->name('delete.category');


Route::get('/pos-settings',[Controllers\PosSettingController::class,'create_pos_settings']);
Route::post('/update-pos-settings',[Controllers\PosSettingController::class,'update_pos_setting'])->name('update.pos.settings');

Route::get('/vat-settings',[Controllers\VatSettingController::class,'create_vat_settings']);
Route::post('/update-vat-settings',[Controllers\VatSettingController::class,'update_vat_setting'])->name('update.vat.settings');

